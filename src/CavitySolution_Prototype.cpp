#include <cmath>
#include <filesystem>
#include <iostream>
#include <random>

#include <boost/asio/thread_pool.hpp>
#include <boost/asio/post.hpp>

#include <deal.II/base/timer.h>
#include <deal.II/base/function.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/mapping_q_generic.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_dgp.h>
#include <deal.II/fe/fe_interface_values.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/vector_tools.h>

#include <deal.II/meshworker/mesh_loop.h>

#include "AssemblerTE.h"
#include "IsotropicConstant.h"
#include "Leapfrog.h"
#include "Leapfrog.hh"
#include "OutputMatrix.h"
#include "NumberSpaces.h"

#include "CavitySolution_QM.h"
#include "DomainIndicatorFunction.h"
#include "Distort.h"
#include "LiftOperator.h"
#include "GenerateDomain.h"

std::mutex gmsh_mutex;

class TimeIndependent_Prototype
{
private:
  void assemble_grid();
  void setup_system();
  void assemble_system();
  void assemble_rhs_parallel(double time_interpolate, dealii::BlockVector<double>& j);
  double interface_error(double time_interpolate, dealii::BlockVector<double>& solution);

  double compute_error_L2(dealii::BlockVector<double> &coeff_vector);
  double compute_L2_norm(dealii::BlockVector<double> &coeff_vector);

  void output_grid(int level);
  void output_final_error();
  void output_error(int timestep_number);
  void output_step(int timestep_number);

  int degree;
  const double mesh_size;
  int refinements;

  bool vtu_output = false;

  int total_timesteps;
  double start_time;
  double end_time;

  // Parameters for Cavity Solution
  // see pdf Cavity_Solution_Time_dependent for parameter relation
  const int k;       // parameter for k_1 on Omega_1
  const int k_tilda; // parameter for k_2 on Omega_2
  const int z_2;     // paramter for k_2
  const double E_2;  // E_2 scalar component

  CavitySolution_QM cavity_solution;
  CavitySolution_QM cavity_solution_tilda;
  std::map<dealii::types::material_id, const dealii::Function<2, double>*> cavity_solution_map;
  CavitySolutionCombined_QM cavity_combined;

  SurfaceCurrent_QM surface_current;

  const double a1{ 2 }; // x-Dimension
  const double a2{ 1 }; // y-Dimension

  

  std::filesystem::path output_path;

  double time = 0;
  double timestep_width;

  double l2_error_nodal = 0;
  double l2_error_modal = 0;
  double l2_error_diff = 0;
  double l2_interface_error = 0;
  double max_l2_error_nodal = 0;
  double max_l2_error_modal = 0;
  double max_l2_error_diff = 0;
  double max_l2_interface_error = 0;

  dealii::Triangulation<2> triangulation;
  dealii::DoFHandler<2> dof_handler;
  dealii::FESystem<2> fe;
  dealii::MappingQ1<2> mapping;
  const dealii::QGauss<2> cell_quadrature;
  const dealii::QGauss<1> face_quadrature;
  const dealii::QGauss<1> error_face_quadrature;
  MaxwellProblem::Data::IsotropicConstant<2> mu;
  MaxwellProblem::Data::IsotropicConstant<2> eps;

  MaxwellProblem::Assembling::AssemblerTE assembler;
  LiftOperator lift_operator;

  // Inv mass matrix
  dealii::BlockSparsityPattern mass_pattern;
  dealii::BlockSparseMatrix<double> mass;
  dealii::BlockSparsityPattern inv_mass_pattern;
  dealii::BlockSparseMatrix<double> inv_mass;
  // Curl matrix
  dealii::BlockSparsityPattern curl_pattern;
  dealii::BlockSparseMatrix<double> curl;
  // Lift matrix
  dealii::BlockSparsityPattern lift_pattern;
  dealii::BlockSparseMatrix<double> lift_matrix;

  // ToDo: Get rid of some of them
  //  Current vector nodal
  dealii::BlockVector<double> j_current_nodal;
  dealii::BlockVector<double> j_current_nodal_space;
  dealii::BlockVector<double> j_current_modal;
  dealii::BlockVector<double> j_current_modal_space;
  // Solution vectors
  dealii::BlockVector<double> solution_nodal;
  dealii::BlockVector<double> solution_modal;
  dealii::BlockVector<double> solution_difference;
  // Temporary vectors
  dealii::BlockVector<double> tmp;

  std::vector<std::pair<double, std::string>> times_and_names;

public:
  TimeIndependent_Prototype(
    int degree,
    const double mesh_size,
    int refinement_level,
    int total_timesteps,
    double start_time,
    double end_time,
    int k,
    int k_tilda,
    int z_2,
    double E_2,
    const std::filesystem::path& output,
    double mu_0,
    double eps_0);

  void run();
  void set_total_timesteps(int new_total_timesteps);
  void set_vtu_output(bool output);
  void assemble();
  void print_mesh_info(std::ostream& stream);
  void output_matrices();
};

// =====================================  CONSTRUCTORS =====================================

TimeIndependent_Prototype::TimeIndependent_Prototype(
  int degree,
  const double mesh_size,
  int refinement_level,
  int total_timesteps,
  double start_time,
  double end_time,
  int k,
  int k_tilda,
  int z_2,
  double E_2,
  const std::filesystem::path& output,
  double mu_0,
  double eps_0)
  : degree(degree),
  mesh_size(mesh_size),
  refinements(refinement_level),
  total_timesteps(total_timesteps),
  start_time(start_time),
  end_time(end_time),
  k{ k },
  k_tilda{ k_tilda },
  z_2{ z_2 },
  E_2{ E_2 },
  cavity_solution(k, z_2, E_2),
  cavity_solution_tilda(k_tilda, z_2, E_2),
  cavity_solution_map{ {0, &cavity_solution}, {1, &cavity_solution_tilda} },
  cavity_combined(k, k_tilda, z_2, E_2),
  surface_current(k, k_tilda, z_2, E_2),
  output_path(output),
  dof_handler(triangulation),
  fe(dealii::FESystem<2>(dealii::FE_DGQ<2>(degree), 1), 1,
    dealii::FESystem<2>(dealii::FE_DGQ<2>(degree), 2), 1),
  cell_quadrature(2 * degree + 2),
  face_quadrature(2 * degree + 1),
  error_face_quadrature(6 * degree + 1),
  mu(mu_0),
  eps(eps_0),
  assembler(
    fe,
    mapping,
    cell_quadrature,
    face_quadrature,
    dof_handler,
    mu,
    eps),
  lift_operator(
    fe, mapping, face_quadrature, dof_handler, mu, eps)
{

  timestep_width = 1. / total_timesteps;

}

// ===================================== HELPING ROUTINE =====================================

void TimeIndependent_Prototype::set_total_timesteps(int new_total_timesteps)
{
  this->total_timesteps = new_total_timesteps;
  timestep_width = 1. / new_total_timesteps;
}

void TimeIndependent_Prototype::set_vtu_output(bool output)
{
  vtu_output = output;
}

void TimeIndependent_Prototype::assemble()
{
  assemble_grid();
  setup_system();
  assemble_system();
}

void TimeIndependent_Prototype::assemble_grid()
{
  const dealii::Point<2> p0(0., 0.);
  const dealii::Point<2> p1(a1/2., a2);
  const dealii::Point<2> p2(a1, a2);

  assemble_grid_xy_distortion(triangulation, p0, p1, p2, mesh_size, 0.0);
  const double distortion_factor = degree % 2 == 1 ? 0 : 0.2;
  random_refiner(triangulation, distortion_factor);
  
  mark_cells(triangulation, 0, 1, 1.0);


  output_grid(0);
  refine_interface(triangulation, refinements);

}

void TimeIndependent_Prototype::setup_system()
{

  // dof handling
  dof_handler.distribute_dofs(fe);
  std::vector<unsigned int> block_components = { 0, 1, 1 };
  dealii::DoFRenumbering::component_wise(dof_handler, block_components);

  // setup matrices
  assembler.generate_mass_pattern(mass, mass_pattern);
  assembler.generate_mass_pattern(inv_mass, inv_mass_pattern);
  assembler.generate_curl_pattern(curl, curl_pattern);

  lift_operator.generate_lift_matrix_pattern(lift_matrix, lift_pattern);

  // setup vectors
  {
    std::vector<dealii::types::global_dof_index> dofs_per_block =
      dealii::DoFTools::count_dofs_per_fe_block(dof_handler, { 0, 1 });

    const auto n_H = dofs_per_block[0];
    const auto n_E = dofs_per_block[1];

    solution_nodal.reinit(2);
    solution_nodal.block(0).reinit(n_H);
    solution_nodal.block(1).reinit(n_E);
    solution_nodal.collect_sizes();
    solution_modal.reinit(solution_nodal);
    solution_difference.reinit(solution_nodal);
    j_current_nodal.reinit(solution_nodal);
    j_current_nodal_space.reinit(solution_nodal);
    j_current_modal.reinit(solution_nodal);
    j_current_modal_space.reinit(solution_nodal);
    tmp.reinit(solution_nodal);
  }
}

void TimeIndependent_Prototype::assemble_system()
{
  assembler.assemble_mass_matrix_parallel(mass, inv_mass);
  assembler.assemble_curl_matrix_parallel(curl);
  curl.block(0, 1).operator*=(-1.0);
  lift_operator.assemble_lift_matrix(lift_matrix);
}

double TimeIndependent_Prototype::compute_error_L2(dealii::BlockVector<double> &coeff_vector)
{
  Vector<double> local_errors(triangulation.n_active_cells());
  const dealii::QGauss<2> quad(2 * degree + 4);

  cavity_solution.set_time(time);
  cavity_solution_tilda.set_time(time);
  DomainIndicatorFunction indicator_m(0);
  DomainIndicatorFunction indicator_p(1);

  dealii::VectorTools::integrate_difference(
    mapping,
    dof_handler,
    coeff_vector,
    cavity_solution,
    local_errors,
    quad,
    dealii::VectorTools::L2_norm,
    &indicator_m);

  double error_m =
    dealii::VectorTools::compute_global_error(triangulation,
      local_errors,
      VectorTools::L2_norm);
  local_errors = 0;
  dealii::VectorTools::integrate_difference(
    mapping,
    dof_handler,
    coeff_vector,
    cavity_solution_tilda,
    local_errors,
    quad,
    dealii::VectorTools::L2_norm,
    &indicator_p);

  double error_p =
    dealii::VectorTools::compute_global_error(triangulation,
      local_errors,
      VectorTools::L2_norm);
  return sqrt(error_m * error_m + error_p * error_p);
}

double TimeIndependent_Prototype::compute_L2_norm(dealii::BlockVector<double> &coeff_vector)
{
  return std::sqrt(mass.matrix_norm_square(coeff_vector));
}

// ===================================== OUTPUT ROUTINE =====================================
void TimeIndependent_Prototype::output_grid(int level)
{
  std::stringstream mesh_size_name;
  mesh_size_name << "mesh_size_" << mesh_size;
  auto grids = output_path / "grids" / mesh_size_name.str();
  if (!std::filesystem::exists(grids))
  {
    std::filesystem::create_directories(grids);
  }
  std::stringstream filename;
  filename << "grid_" << level << ".vtu";
  auto grid_path = grids / filename.str();
  std::ofstream output_file(grid_path);
  dealii::GridOut().write_vtu(triangulation, output_file);
}

void TimeIndependent_Prototype::output_error(int timestep_number)
{

  std::stringstream dir_name;
  dir_name << "degree_" << degree 
    << "/mesh_size_" << mesh_size 
    << "/steps_" << total_timesteps;
  
  auto dir_path = output_path / dir_name.str();
  if (!std::filesystem::exists(dir_path))
  {
    std::filesystem::create_directories(dir_path);
  }

  auto error_file_path = dir_path / "error_steps.txt";
  const auto exists = std::filesystem::exists(error_file_path);
  std::ofstream error_file(error_file_path, std::ios_base::app);

  if(!exists)
    error_file << "time_step_number"
      << "\t" << "L2_error_modal"
      << "\t" << "L2_error_nodal"
      << "\t" << "L2_error_diff"
      << "\t" << "min_mesh_size"
      << "\t" << "max_mesh_size";

  error_file << timestep_number
    << "\t" << std::setw(12)  << l2_error_modal
    << "\t" << std::setw(12) << l2_error_nodal
    << "\t" << std::setw(12) << l2_error_diff
    << "\t" << std::setw(12) << GridTools::minimal_cell_diameter(triangulation)
    << "\t" << std::setw(12) << GridTools::maximal_cell_diameter(triangulation)
    << std::endl;
}

void TimeIndependent_Prototype::output_step(int timestep_number)
{

  std::stringstream dir_name;
  dir_name << "degree_" << degree 
    << "/mesh_size_" << mesh_size 
    << "/steps_" << total_timesteps
    << "/solution";
  auto dir_path = output_path / dir_name.str();
  if (!std::filesystem::exists(dir_path))
  {
    std::filesystem::create_directories(dir_path);
  }

  std::stringstream filename;
  filename << "solution_" << timestep_number << ".vtu";

  auto solution_path = dir_path / filename.str();
  auto pair_path = dir_path / "solution.pvd";

  std::ofstream solution_stream(solution_path);
  std::ofstream pair_stream(pair_path);

  std::vector<std::string> solution_names = { "H", "E", "E" };
  std::vector<DataComponentInterpretation::DataComponentInterpretation>
    interpretation = {
        DataComponentInterpretation::component_is_scalar,
        DataComponentInterpretation::component_is_part_of_vector,
        DataComponentInterpretation::component_is_part_of_vector };

  dealii::DataOut<2> data_out;
  data_out.attach_dof_handler(dof_handler);
  data_out.add_data_vector(dof_handler, solution_nodal, solution_names, interpretation);
  data_out.build_patches(3);

  data_out.write_vtu(solution_stream);
  times_and_names.emplace_back(time, filename.str());
  DataOutBase::write_pvd_record(pair_stream, times_and_names);
}

void TimeIndependent_Prototype::output_final_error()
{
  auto error_file_path = output_path / "error.txt";
  const auto exists = std::filesystem::exists(error_file_path);
  std::ofstream error_file(error_file_path, std::ios_base::app);

  if(!exists)
    error_file
      << "degree"
      << "\t" << "edge_refinements"
      << "\t" << "start_time"
      << "\t" << "end_time"
      << "\t" << "min_mesh_size"
      << "\t" << "max_mesh_size"
      << "\t" << "time_step_width"
      << "\t" << "max_l2_error_modal"
      << "\t" << "max_l2_error_nodal"
      << "\t" << "max_l2_error_diff"
      << std::endl;

  error_file << degree
    << "\t" << refinements
    << "\t" << start_time
    << "\t" << end_time
    << "\t" << std::setw(12) << GridTools::minimal_cell_diameter(triangulation)
    << "\t" << std::setw(12) << GridTools::maximal_cell_diameter(triangulation)
    << "\t" << std::setw(12) << timestep_width
    << "\t" << std::setw(12) << max_l2_error_modal
    << "\t" << std::setw(12) << max_l2_error_nodal
    << "\t" << std::setw(12) << max_l2_error_diff
    << std::endl;
}

void TimeIndependent_Prototype::output_matrices()
{

  std::stringstream path_extension;
  path_extension << "degree_" << degree 
    << "/mesh_size_" << mesh_size 
    << "/matrices";
  const auto matrix_path = output_path / path_extension.str();
  std::filesystem::create_directories(matrix_path);

  // mass matrix
  {
    const auto out_file_path = matrix_path / "mass_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(mass, out_file);
  }

  // inv mass matrix
  {
    const auto out_file_path = matrix_path / "inv_mass_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(inv_mass, out_file);
  }

  // curl matrix
  {
    const auto out_file_path = matrix_path / "curl_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(curl, out_file);
  }

  // lift matrix
  {
    const auto out_file_path = matrix_path / "lift_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(lift_matrix, out_file);
  }
}

void TimeIndependent_Prototype::print_mesh_info(std::ostream& stream)
{
  stream << "Mesh Info: \n"
    << std::string(20, '_') << std::endl
    << " minimal cell diameter: " << GridTools::minimal_cell_diameter(triangulation) << "\n"
    << " maximal cell diameter: " << GridTools::maximal_cell_diameter(triangulation) << "\n"
    << "       number of cells: " << triangulation.n_cells() << "\n"
    << "number of active cells: " << triangulation.n_active_cells() << "\n"
    << "number of active faces: " << triangulation.n_active_faces() << "\n"
    << "                  DoFs: " << dof_handler.n_dofs() << "\n"
    << std::string(20, '_') << std::endl;
}

void TimeIndependent_Prototype::run()
{
  // reset values
  j_current_nodal = 0;
  j_current_nodal_space = 0;
  j_current_modal = 0;
  j_current_modal_space = 0;

  l2_error_nodal = 0.0;
  max_l2_error_nodal = 0.0;
  l2_error_modal = 0.0;
  max_l2_error_modal = 0.0;
  l2_error_diff = 0.0;
  max_l2_error_diff = 0.0;
  l2_interface_error = 0.0;
  max_l2_interface_error = 0.0;
  time = start_time;
  int timestep_number = 0;

  // initial value with cavity solution
  cavity_solution.set_time(start_time);
  cavity_solution_tilda.set_time(start_time);
  cavity_combined.set_time_combined(start_time);

  // Interpolation for the nodal initial value
  solution_nodal = 0;
  dealii::VectorTools::interpolate_based_on_material_id(
    mapping, dof_handler, cavity_solution_map, solution_nodal);

  // Projection for the modal initial value
  dealii::AffineConstraints constraints;
  constraints.close();
  dealii::VectorTools::project(mapping, dof_handler, constraints, cell_quadrature, cavity_combined, solution_modal);

  // Compute difference
  solution_difference = solution_nodal;
  solution_difference -= solution_modal;

  // output initial error
  l2_error_nodal = compute_error_L2(solution_nodal);
  l2_error_modal = compute_error_L2(solution_modal);
  l2_error_diff = compute_L2_norm(solution_difference);
  max_l2_error_nodal = l2_error_nodal > max_l2_error_nodal ? l2_error_nodal : max_l2_error_nodal;
  max_l2_error_modal = l2_error_modal > max_l2_error_modal ? l2_error_modal : max_l2_error_modal;
  max_l2_error_diff = l2_error_diff > max_l2_error_diff ? l2_error_diff : max_l2_error_diff;
  //l2_interface_error = interface_error(start_time, solution);
  //max_l2_interface_error = l2_interface_error > max_l2_interface_error ? l2_interface_error : max_l2_interface_error;
  if (vtu_output)
    output_step(timestep_number);
  output_error(timestep_number);
  std::cout << "Initial error nodal:            " << l2_error_nodal << "\n";
  std::cout << "Initial error modal:            " << l2_error_modal << "\n";
  std::cout << "Initial error diff :            " << l2_error_diff  << "\n";
  //std::cout << "Initial interface error:  " << l2_interface_error << "\n";

  // initialise time integrator
  MaxwellProblem::TimeIntegration::Leapfrog<SparseMatrix<double>, SparseMatrix<double>>
    integrator_nodal(
      inv_mass.block(0, 0),
      inv_mass.block(1, 1),
      curl.block(1, 0),
      curl.block(0, 1),
      timestep_width);
  
  MaxwellProblem::TimeIntegration::Leapfrog<SparseMatrix<double>, SparseMatrix<double>>
    integrator_modal(
      inv_mass.block(0, 0),
      inv_mass.block(1, 1),
      curl.block(1, 0),
      curl.block(0, 1),
      timestep_width);

  time += timestep_width;
  timestep_number++;

  dealii::Timer eta_timer;
  eta_timer.start();

  // Nodal Interpolation Setup
  dealii::FEValuesExtractors::Vector E(1);
  dealii::ComponentMask E_mask = fe.component_mask(E);
  FunctionLift func_lift{surface_current};
  dealii::VectorTools::interpolate(dof_handler, func_lift, j_current_nodal, E_mask);
  lift_matrix.vmult(j_current_nodal_space, j_current_nodal);
  
  // Modal Projection Setup
  lift_operator.assemble_modal_rhs(surface_current, j_current_modal_space);

  const int error_steps = 800; // calculate error in every step

  for (; timestep_number <= (end_time - start_time) * total_timesteps; time += timestep_width, timestep_number++)
  {
    // TIME INTEGRATION
    surface_current.set_time(time-timestep_width / 2);
    auto time_factor = surface_current.get_time_factor();
    
    // Nodal Interpolation
    j_current_nodal.block(1) = j_current_nodal_space.block(1);
    j_current_nodal.block(1) *= time_factor;

    // Modal Projection
    j_current_modal.block(1) = j_current_modal_space.block(1);
    j_current_modal.block(1) *= time_factor;
    
    // Nodal Integration Step
    integrator_nodal.integrate_step(solution_nodal.block(0), solution_nodal.block(1), j_current_nodal.block(1));
    
    // Modal Integration Step
    integrator_modal.integrate_step(solution_modal.block(0), solution_modal.block(1), j_current_modal.block(1));

    // PROJECTION
    /**
    if (timestep_number % error_steps == 0 || timestep_number == static_cast<int>((end_time - start_time) * total_timesteps)) {
      // Interpolate
      cavity_solution.set_time(time);
      cavity_solution_tilda.set_time(time);
      dealii::VectorTools::interpolate_based_on_material_id(
          dealii::MappingQGeneric<2, 2>(1), dof_handler, cavity_solution_map, solution);
      output_step(timestep_number);
    }
    **/

    if (timestep_number % error_steps == 0 || timestep_number == static_cast<int>((end_time - start_time) * total_timesteps))
    {
      solution_difference = solution_nodal;
      solution_difference -= solution_modal;

      l2_error_nodal = compute_error_L2(solution_nodal);
      l2_error_modal = compute_error_L2(solution_modal);
      l2_error_diff = compute_L2_norm(solution_difference);
      max_l2_error_nodal = l2_error_nodal > max_l2_error_nodal ? l2_error_nodal : max_l2_error_nodal;
      max_l2_error_modal = l2_error_modal > max_l2_error_modal ? l2_error_modal : max_l2_error_modal;
      max_l2_error_diff = l2_error_diff > max_l2_error_diff ? l2_error_diff : max_l2_error_diff;
      //l2_interface_error = interface_error(time, solution);
      //max_l2_interface_error = l2_interface_error > max_l2_interface_error ? l2_interface_error : max_l2_interface_error;

      eta_timer.stop();
      auto eta_hours = ((eta_timer.wall_time() / error_steps) 
        * ((end_time - start_time) * total_timesteps - timestep_number)) / (60 * 60);
      std::cout << "timestep: "
        << timestep_number << "/" << (end_time - start_time) * total_timesteps
        << "\t error nodal: " << l2_error_nodal
        << "\t error modal: " << l2_error_modal
        << "\t error diff:  " << l2_error_diff
        //<< "\t interface error: " << l2_interface_error
        << "\t eta: " << eta_hours * 60 << " min"
        << "\n"
        << std::flush;

      if (vtu_output)
        output_step(timestep_number);
      output_error(timestep_number);
      eta_timer.restart();
    }
  }

  if (vtu_output)
    output_step(timestep_number);
  output_final_error();
}

int main(int /*argc*/, char** /* argv*/)
{

  const int num_threads = 5;
  //const int num_threads = 1;
  boost::asio::thread_pool pool(num_threads);


  const double start_time = 0.0;
  const double end_time = 1.0;

  const double mesh_upper = 0.1;
  const double mesh_lower = 0.01;
  const int points = 20;

  std::vector<int> degrees{1,2,3,4};
  auto mesh_range = MaxwellProblem::Tools::log_spaced(mesh_lower, mesh_upper, points);
  std::reverse(mesh_range.begin(), mesh_range.end());
  std::vector<int> edge_refinements{0};
  std::vector<int> timesteps{10000};


  // Cavity solution parameter
  const int k = 2;
  const int k_tilda = 4;
  const int z_2 = 1;
  const double E_2 = 1;

  const double mu = 1.0;
  const double eps = 1.0;

  auto output_path = std::filesystem::current_path();
  if (!std::filesystem::exists(output_path))
  {
    std::filesystem::create_directories(output_path);
  }

  //int skip = 0;
  for (auto& deg : degrees)
  {

    for (auto& ref : edge_refinements)
    {
      for (auto& mesh_h : mesh_range)
      { 
          //if (skip < 9) {
          //  skip++;
          //  continue;
          //}

          boost::asio::post(pool,
          [mesh_h, deg, ref, output_path, timesteps, start_time, end_time,
            k, k_tilda, z_2, E_2, mu, eps](){

            TimeIndependent_Prototype prototype(deg,
              mesh_h,
              ref,
              1,  // total timesteps
              start_time,  // start_time
              end_time,
              k,
              k_tilda,
              z_2,
              E_2,
              output_path,
              mu,
              eps);
            prototype.assemble();
            //prototype.output_matrices();
            //prototype.set_vtu_output(false);

            for (auto& timestep : timesteps)
            {
              prototype.set_total_timesteps(timestep);
              prototype.print_mesh_info(std::cout);
              prototype.run();
            }
          });

      }
    }
  }

  pool.join();

  return 0;
}
