//
// Created by julian on 4/22/22.
//

#ifndef QM_PROTOTYPES_CAVITYSOLUTION_CAVITYSOLUTION_H_
#define QM_PROTOTYPES_CAVITYSOLUTION_CAVITYSOLUTION_H_

#include "deal.II/base/function.h"
#include "math.h"
#include "iostream"

class CavitySolution_QM : public dealii::Function<2, double> {

 public:

  /**
   * See pdf Cavity_Solution_Time_dependent for paramater relations
   *
   * @param k: parameter for k_1 on \Omega_1 or \Omega_2
   * @param z_2: parameter for  k_2
   * @param E_2: E_2 scalar component
   */
  CavitySolution_QM(int k, int z_2, double E_2, double initial_time = 0.0);

  double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;

 private:

  // given values
  const int k;
  const int z_2;
  const double E_2;

  // calculated values
  const double k_1;
  const double k_2;

  const double E_1;
  const double omega;

};

class CavitySolutionCombined_QM : public dealii::Function<2, double> {

 public:

  /**
   * See pdf Cavity_Solution_Time_dependenßt for paramater relations
   *
   * @param k: parameter for k_1 on \Omega_1 or \Omega_2
   * @param z_2: parameter for  k_2
   * @param E_2: E_2 scalar component
   */
  CavitySolutionCombined_QM(int k, int k_tilda, int z_2, double E_2, double initial_time = 0.0);
  void set_time_combined(const double time);
  double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;

 private:

  CavitySolution_QM solution;
  CavitySolution_QM solution_tilda;

};

class SurfaceCurrent_QM : public dealii::Function<2, double> {
 public:

  /**
   * See pdf Cavity_Solution_Time_dependent for parameter relations
   *
   * @param k: parameter for k_1 on \Omega_1
   * @param k_tilda: parameter for k_1 on \Omega_2
   * @param z_2: parameter for  k_2
   * @param E_2: E_2 scalar component
   */

  SurfaceCurrent_QM(int k, int k_tilda, int z_2, double E_2, double initial_time = 0.0);

  double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;
  double get_time_factor() const;

 private:

  // given values
  const int k;
  const int k_tilda;
  const int z_2;
  const double E_2;

  // calculated values
  const double k_1;
  const double k_1_tilda;
  const double k_2;

  const double E_1;
  const double E_1_tilda;
  const double omega;
  const double omega_tilda;

};

class SurfaceCorrection_QM : public dealii::Function<2, double> {
 public:

  /**
   * See pdf Cavity_Solution_Time_dependent for parameter relations
   *
   * @param k: parameter for k_1 on \Omega_1
   * @param k_tilda: parameter for k_1 on \Omega_2
   * @param z_2: parameter for  k_2
   * @param E_2: E_2 scalar component
   */

  SurfaceCorrection_QM(int k, int k_tilda, int z_2, double E_2, double initial_time = 0.0);

  double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;

 private:

  // given values
  const int k;
  const int k_tilda;
  const int z_2;
  const double E_2;

  // calculated values
  const double k_1;
  const double k_1_tilda;
  const double k_2;

  const double E_1;
  const double E_1_tilda;
  const double omega;
  const double omega_tilda;

};

class FunctionLift : public dealii::Function<2, double> {
  public:
    FunctionLift(dealii::Function<2, double>& func) : Function(3), func{func} {};

    double value(const dealii::Point<2> &p, const unsigned int component = 0) const override
    {
      switch (component)
      {
      case 0:
        return 0.;
        break;
      case 1:
        return func.value(p, 0);
        break;
      case 2:
        return func.value(p, 1);
        break;
      default:
        throw dealii::ExcMessage("Lifted function only has 3 components.");
      }
    }  
    dealii::Function<2, double>& func;
};


#endif //QM_PROTOTYPES_CAVITYSOLUTION_CAVITYSOLUTION_H_
