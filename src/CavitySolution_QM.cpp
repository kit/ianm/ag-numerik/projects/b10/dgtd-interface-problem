//
// Created by julian on 4/22/22.
//

#include "CavitySolution_QM.h"

CavitySolution_QM::CavitySolution_QM(
    int k,
    int z_2,
    double E_2,
    double initial_time) :
    Function(3, initial_time),
    k{k},
    z_2{z_2},
    E_2{E_2},
    k_1{(M_PI * k) / 2},
    k_2{M_PI * z_2},
    E_1{-(E_2 * k_2) / k_1},
    omega{sqrt(k_1 * k_1 + k_2 * k_2)} {

  if (abs(E_1 * k_1 + E_2 * k_2) > 1e-9)
    std::cerr << "(k_1, k_2) is not orthogonal to (E_1, E_2)\n";

}

double CavitySolution_QM::value(const dealii::Point<2> &p, const unsigned int component) const {

  // H_3 component
  if (component == 0) {
    return ((k_1 * E_2 - k_2 * E_1) * cos(k_2 * p[1]) * cos(k_1 * p[0]) * sin(omega * get_time())) / omega;
  }
    // E_1 component
  else if (component == 1) {
    return -E_1 * sin(k_2 * p[1]) * cos(k_1 * p[0]) * cos(omega * get_time());
  }
    // E_2 component
  else{
    return -E_2 * cos(k_2 * p[1]) * sin(k_1 * p[0]) * cos(omega * get_time());
  }

}

CavitySolutionCombined_QM::CavitySolutionCombined_QM(
    int k,
    int k_tilda,
    int z_2,
    double E_2,
    double initial_time) :
    Function(3, initial_time),
    solution{k, z_2, E_2, initial_time},
    solution_tilda(k_tilda, z_2, E_2, initial_time){
}

void CavitySolutionCombined_QM::set_time_combined(const double time){
  this->set_time(time);
  solution.set_time(time);
  solution_tilda.set_time(time);
}

double CavitySolutionCombined_QM::value(const dealii::Point<2> &p, const unsigned int component) const {

  auto x_1 = p[0];

  if (x_1 < 1) {
    return solution.value(p, component);
  }
  else if (x_1 > 1) {
    return solution_tilda.value(p, component);
  }
  else {
    std::cout << "Peng!\n";
    return 0;//(solution.value(p, component) + solution_tilda.value(p, component))/2;
  }
}

SurfaceCurrent_QM::SurfaceCurrent_QM(int k,
                                     int k_tilda,
                                     int z_2,
                                     double E_2,
                                     double initial_time) :
    Function(2, initial_time),
    k{k},
    k_tilda{k_tilda},
    z_2{z_2},
    E_2{E_2},
    k_1{(M_PI * k) / 2},
    k_1_tilda{(M_PI * k_tilda) / 2},
    k_2{M_PI * z_2},
    E_1{-(E_2 * k_2) / k_1},
    E_1_tilda{-(E_2 * k_2) / k_1_tilda},
    omega{sqrt(k_1 * k_1 + k_2 * k_2)},
    omega_tilda{sqrt(k_1_tilda * k_1_tilda + k_2 * k_2)}{

  if (abs(E_1 * k_1 + E_2 * k_2) > 1e-9)
    std::cerr << "(k_1, k_2) is not orthogonal to (E_1, E_2)\n";

  if (abs(E_1_tilda * k_1_tilda + E_2 * k_2) > 1e-9)
    std::cerr << "(k_1_tilda, k_2) is not orthogonal to (E_1_tilda, E_2)\n";

}

/*Python Protoype:
*
* def j_surf(x2, t):
*  factor = ((k1_p*E2 - k2*E1_p)*np.cos(k1_p*inter)*np.sin(omega_p*t)/omega_p
*            - (k1_m*E2 - k2*E1_m)*np.cos(k1_m*inter)*np.sin(omega_m*t)/omega_m)
*  return -factor*np.cos(k2*x2)
*/

double SurfaceCurrent_QM::value(const dealii::Point<2> &p, const unsigned int component) const {
  if (component == 0) {
    return 0;
  }

  auto x_2 = p[1];

  return cos(k_2 * x_2);
}

double SurfaceCurrent_QM::get_time_factor() const {
  auto time = get_time();

  double factor_tilda = (k_1_tilda * E_2 - k_2 * E_1_tilda) * cos(k_1_tilda) * sin(omega_tilda * time)
      / omega_tilda;
  double factor = (k_1 * E_2 - k_2 * E_1) * cos(k_1) * sin(omega * time)
      / omega;
    
  return -(factor - factor_tilda);
}


SurfaceCorrection_QM::SurfaceCorrection_QM(int k,
                                     int k_tilda,
                                     int z_2,
                                     double E_2,
                                     double initial_time) :
    Function(2, initial_time),
    k{k},
    k_tilda{k_tilda},
    z_2{z_2},
    E_2{E_2},
    k_1{(M_PI * k) / 2},
    k_1_tilda{(M_PI * k_tilda) / 2},
    k_2{M_PI * z_2},
    E_1{-(E_2 * k_2) / k_1},
    E_1_tilda{-(E_2 * k_2) / k_1_tilda},
    omega{sqrt(k_1 * k_1 + k_2 * k_2)},
    omega_tilda{sqrt(k_1_tilda * k_1_tilda + k_2 * k_2)}{

  if (abs(E_1 * k_1 + E_2 * k_2) > 1e-9)
    std::cerr << "(k_1, k_2) is not orthogonal to (E_1, E_2)\n";

  if (abs(E_1_tilda * k_1_tilda + E_2 * k_2) > 1e-9)
    std::cerr << "(k_1_tilda, k_2) is not orthogonal to (E_1_tilda, E_2)\n";

}

/*
 * def j_correction(x2, t):
    return np.array([-(E1_p * np.cos(k1_p) * np.cos(omega_p*t) - E1_m * np.cos(k1_m) * np.cos(omega_m * t))*np.sin(k2*x2),
                    -(np.sin(k1_p) * np.cos(omega_p*t) - np.sin(k1_m) * np.cos(omega_m*t)) * E2 * np.cos(k2*x2)])

 */
double SurfaceCorrection_QM::value(const dealii::Point<2> &p, const unsigned int component) const {

  auto time = get_time();
  auto x_2 = p[1];

  if (component == 0) {
    auto factor_tilda = E_1_tilda * cos(k_1_tilda) * cos(omega_tilda * time);
    auto factor = E_1 * cos(k_1) * cos(omega * time);

    return - (factor_tilda - factor) * sin(k_2 * x_2);
  }
  else if (component == 1) {
    auto factor_tilda = sin(k_1_tilda) * cos(omega_tilda * time);
    auto factor = sin(k_1) * cos(omega * time);

    return - (factor_tilda - factor) * E_2 * cos(k_2 * x_2);
  }

  return 0;
}
