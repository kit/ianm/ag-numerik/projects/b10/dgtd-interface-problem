#include <cmath>
#include <filesystem>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <set>
#include <exception>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
namespace pt = boost::property_tree;
#include <boost/asio/thread_pool.hpp>
#include <boost/asio/post.hpp>


#include <deal.II/base/timer.h>
#include <deal.II/base/function.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/mapping_q_generic.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_interface_values.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/vector_tools.h>

#include <deal.II/meshworker/mesh_loop.h>

#include "AssemblerTE.h"
#include "IsotropicConstant.h"
#include "Leapfrog.h"
#include "Leapfrog.hh"
#include "CrankNicolson.h"
#include "CrankNicolson.hh"
#include "OutputMatrix.h"
#include "NumberSpaces.h"

#include "DomainIndicatorFunction.h"
#include "ReferenceSolution.h"
#include "ReferenceSolution.hh"
#include "FourierCurrent.h"
#include "Distort.h"
#include "LiftOperator.h"
#include "CavitySolution_QM.h"
#include "GenerateDomain.h"

enum ModeSelection {
  Modal,
  Nodal,
  Difference
};


class ReferenceSolution_Prototype
{
private:
  void assemble_grid();
  void assemble_reference_grid();
  void setup_system();
  void assemble_system();

  double compute_error_L2(dealii::BlockVector<double> &coeff_vector);
  double compute_L2_norm(dealii::BlockVector<double> &coeff_vector);

  void output_grid(std::string name);
  void output_final_error();
  void output_error(int timestep_number);
  void output_step(int timestep_number);

  int degree;
  double mesh_size;
  int refinements;

  bool vtu_output = false;

  double timestep_width;
  double start_time;
  double end_time;
  const int error_ratio;
  int total_timesteps;


  const double a1{ 2 }; // x-Dimension
  const double a2{ 1 }; // y-Dimension

  std::filesystem::path output_path;

  double time = 0;

  double l2_error = 0;
  double max_l2_error = 0;

  dealii::Triangulation<2> triangulation;
  dealii::DoFHandler<2> dof_handler;
  dealii::FESystem<2> fe;
  dealii::MappingQ1<2> mapping;
  const dealii::QGauss<2> cell_quadrature;
  const dealii::QGauss<1> face_quadrature;
  MaxwellProblem::Data::IsotropicConstant<2> mu;
  MaxwellProblem::Data::IsotropicConstant<2> eps;
  const ModeSelection mode;

  const std::function<double(double)> time_modulation;

  ReferenceSolution<2, dealii::BlockVector<double>>& ref_sol;
  bool save_ref_sol;

  FourierCurrent& surface_current;

  MaxwellProblem::Assembling::AssemblerTE assembler;
  LiftOperator lift_operator;

  // Inv mass matrix
  dealii::BlockSparsityPattern mass_pattern;
  dealii::BlockSparseMatrix<double> mass;
  dealii::BlockSparsityPattern inv_mass_pattern;
  dealii::BlockSparseMatrix<double> inv_mass;
  // Curl matrix
  dealii::BlockSparsityPattern curl_pattern;
  dealii::BlockSparseMatrix<double> curl;
  // Lift matrix
  dealii::BlockSparsityPattern lift_pattern;
  dealii::BlockSparseMatrix<double> lift_matrix;

  // ToDo: Get rid of some of them
  //  Current vector
  dealii::BlockVector<double> j_current_nodal_space;
  dealii::BlockVector<double> j_current_nodal;
  dealii::BlockVector<double> j_current_modal_space;
  dealii::BlockVector<double> j_current_modal;
  // Solution vectors
  dealii::BlockVector<double> solution_nodal;
  dealii::BlockVector<double> solution_modal;
  dealii::BlockVector<double> solution_difference;
  // tmp vector
  dealii::BlockVector<double> tmp;

  std::vector<std::pair<double, std::string>> times_and_names;

public:
  ReferenceSolution_Prototype(
    int degree,
    double mesh_size,
    int refinement_level,
    double time_step_width,
    double start_time,
    double end_time,
    int error_ratio,
    const std::filesystem::path& output,
    double mu_0,
    double eps_0,
    const ModeSelection mode,
    std::function<double(double)> time_modulation,
    ReferenceSolution<2, dealii::BlockVector<double>>& ref_sol,
    bool save_ref_sol,
    FourierCurrent& surface_current);

  void run();
  void set_total_timesteps(int new_total_timesteps);
  void set_vtu_output(bool output);
  void assemble();
  void print_mesh_info(std::ostream& stream);
  void output_matrices();
};

// =====================================  CONSTRUCTORS =====================================

ReferenceSolution_Prototype::ReferenceSolution_Prototype(
  int degree,
  double mesh_size,
  int refinement_level,
  double time_step_width,
  double start_time,
  double end_time,
  int error_ratio,
  const std::filesystem::path& output,
  double mu_0,
  double eps_0,
  const ModeSelection mode,
  std::function<double(double)> time_modulation,
  ReferenceSolution<2, dealii::BlockVector<double>>& ref_sol,
  bool save_ref_sol,
  FourierCurrent& surface_current)
  : degree(degree),
  mesh_size(mesh_size),
  refinements(refinement_level),
  timestep_width(time_step_width),
  start_time(start_time),
  end_time(end_time),
  error_ratio(error_ratio),
  total_timesteps(std::round((end_time - start_time)/timestep_width)),
  output_path(output),
  dof_handler(triangulation),
  fe(dealii::FESystem<2>(FE_DGQ<2>(degree), 1), 1,
    dealii::FESystem<2>(FE_DGQ<2>(degree), 2), 1),
  cell_quadrature(2 * degree + 2),
  face_quadrature(2 * degree + 1),
  mu(mu_0),
  eps(eps_0),
  mode(mode),
  time_modulation(time_modulation),
  ref_sol(ref_sol),
  save_ref_sol(save_ref_sol),
  surface_current(surface_current),
  assembler(
    fe,
    mapping,
    cell_quadrature,
    face_quadrature,
    dof_handler,
    mu,
    eps),
  lift_operator(
    fe, mapping, face_quadrature, dof_handler, mu, eps)
{}

// ===================================== HELPING ROUTINE =====================================

void ReferenceSolution_Prototype::set_total_timesteps(int new_total_timesteps)
{
  this->total_timesteps = new_total_timesteps;
  timestep_width = 1. / new_total_timesteps;
}

void ReferenceSolution_Prototype::set_vtu_output(bool output)
{
  vtu_output = output;
}

void ReferenceSolution_Prototype::assemble()
{
  if(save_ref_sol)
    assemble_reference_grid();
  else
    assemble_grid();
  setup_system();
  assemble_system();
}

void ReferenceSolution_Prototype::assemble_grid()
{

  const dealii::Point<2> p0{0,0};
  const dealii::Point<2> p1{a1 / 2., a2};
  const dealii::Point<2> p2{a1, a2};

  assemble_grid_xy_distortion(triangulation, p0, p1, p2, mesh_size, 0.0);
  mark_cells(triangulation, 0, 1, 1.0);

  const double distortion_factor = degree % 2 == 1 ? 0 : 0.2;
  random_refiner(triangulation, distortion_factor);



  //if (degree % 2 == 0)
  //  random_refiner(triangulation, 0.1);

  std::stringstream ss;
  ss << "mesh_size_" << mesh_size;
  output_grid(ss.str());
}

void ReferenceSolution_Prototype::assemble_reference_grid()
{

  const dealii::Point<2> p0{0,0};
  const dealii::Point<2> p1{a1 / 2, a2};
  const dealii::Point<2> p2{a1, a2};

  assemble_grid_xy_distortion(triangulation, p0, p1, p2, mesh_size, 0.0);
  mark_cells(triangulation, 0, 1, 1.0);
  refine_interface(triangulation, refinements, 0, 1, 1.0);

  output_grid("reference");
}

void ReferenceSolution_Prototype::setup_system()
{

  // dof handling

  auto dof_setup_func = [&](dealii::DoFHandler<2>& dof_h) {
    dof_h.distribute_dofs(fe);
    std::vector<unsigned int> block_components = { 0, 1, 1 };
    dealii::DoFRenumbering::component_wise(dof_h, block_components);
  };

  dof_setup_func(dof_handler);

  if (save_ref_sol) {
    ref_sol.initialize(triangulation, dof_setup_func);
  }

  // setup matrices
  assembler.generate_mass_pattern(mass, mass_pattern);
  assembler.generate_mass_pattern(inv_mass, inv_mass_pattern);
  assembler.generate_curl_pattern(curl, curl_pattern);

  lift_operator.generate_lift_matrix_pattern(lift_matrix, lift_pattern);

  // setup vectors
  {
    std::vector<dealii::types::global_dof_index> dofs_per_block =
      dealii::DoFTools::count_dofs_per_fe_block(dof_handler, { 0, 1 });

    const auto n_H = dofs_per_block[0];
    const auto n_E = dofs_per_block[1];

    solution_nodal.reinit(2);
    solution_nodal.block(0).reinit(n_H);
    solution_nodal.block(1).reinit(n_E);
    solution_nodal.collect_sizes();
    solution_modal.reinit(solution_nodal);
    solution_difference.reinit(solution_nodal);
    j_current_nodal_space.reinit(solution_nodal);
    j_current_nodal.reinit(solution_nodal);
    j_current_modal_space.reinit(solution_nodal);
    j_current_modal.reinit(solution_nodal);

    solution_modal.collect_sizes();
    solution_difference.collect_sizes();
    j_current_nodal_space.collect_sizes();
    j_current_nodal.collect_sizes();
    j_current_modal_space.collect_sizes();
    j_current_modal.collect_sizes();
  }
}

void ReferenceSolution_Prototype::assemble_system()
{
  assembler.assemble_mass_matrix_parallel(mass, inv_mass);
  assembler.assemble_curl_matrix_parallel(curl);
  curl.block(0, 1).operator*=(-1.0);
  lift_operator.assemble_lift_matrix(lift_matrix);
}

double ReferenceSolution_Prototype::compute_error_L2(dealii::BlockVector<double> &coeff_vector)
{

  dealii::Functions::FEFieldFunction<2, dealii::BlockVector<double>>
    solution_func(dof_handler, coeff_vector);

  return ref_sol.calculate_error(time, solution_func, dealii::VectorTools::L2_norm, 3 * 3 + 2);

  //return ref_sol.calculate_error_local_dof(
  //  time,
  //  solution,
  //  dof_handler,
  //  dealii::VectorTools::L2_norm,
  //  3*3+2
  //);
}

double ReferenceSolution_Prototype::compute_L2_norm(dealii::BlockVector<double> &coeff_vector)
{

    Vector<double> local_errors(triangulation.n_active_cells());
    const dealii::QGauss<2> quad(6 * degree + 2);
    const dealii::Functions::ZeroFunction<2> zero_func(3); 

    dealii::VectorTools::integrate_difference(
      mapping,
      dof_handler,
      coeff_vector,
      zero_func,
      local_errors,
      quad,
      dealii::VectorTools::L2_norm);

  return dealii::VectorTools::compute_global_error(triangulation,
                                                   local_errors,
                                                   VectorTools::L2_norm);
}

// ===================================== OUTPUT ROUTINE =====================================
void ReferenceSolution_Prototype::output_grid(std::string name)
{
  auto grids = output_path / "grids";
  if (!std::filesystem::exists(grids))
  {
    std::filesystem::create_directory(grids);
  }
  std::stringstream filename;
  filename << "grid_" << name << ".vtk";
  auto grid_path = grids / filename.str();
  std::ofstream output_file(grid_path);
  dealii::GridOut().write_vtk(triangulation, output_file);
}

void ReferenceSolution_Prototype::output_step(int timestep_number)
{

  std::stringstream dir_name;
  dir_name << "output/degree_" << degree << "/mesh_" << mesh_size << "/steps_" << total_timesteps;
  auto dir_path = output_path / dir_name.str();
  if (!std::filesystem::exists(dir_path))
  {
    std::filesystem::create_directories(dir_path);
  }

  std::stringstream filename;
  filename << "solution_" << timestep_number << ".vtu";

  auto solution_path = dir_path / filename.str();
  auto pair_path = dir_path / "solution.pvd";

  std::ofstream solution_stream(solution_path);
  std::ofstream pair_stream(pair_path);

  std::vector<std::string> solution_names = { "H", "E", "E" };
  std::vector<DataComponentInterpretation::DataComponentInterpretation>
    interpretation = {
        DataComponentInterpretation::component_is_scalar,
        DataComponentInterpretation::component_is_part_of_vector,
        DataComponentInterpretation::component_is_part_of_vector };

  dealii::DataOut<2> data_out;
  data_out.attach_dof_handler(dof_handler);

  switch (mode)
  {
  case Nodal:
    data_out.add_data_vector(dof_handler, solution_nodal, solution_names, interpretation);
    break;
  case Modal:
    data_out.add_data_vector(dof_handler, solution_modal, solution_names, interpretation);
    break;
  case Difference:
    data_out.add_data_vector(dof_handler, solution_difference, solution_names, interpretation);
    break;
  }
  data_out.build_patches(3);

  data_out.write_vtu(solution_stream);
  times_and_names.emplace_back(time, filename.str());
  DataOutBase::write_pvd_record(pair_stream, times_and_names);
}

void ReferenceSolution_Prototype::output_error(int timestep_number)
{

  std::stringstream dir_name;
  dir_name << "output/degree_" << degree << "/mesh_" << mesh_size << "/steps_" << total_timesteps;
  auto dir_path = output_path / dir_name.str();
  if (!std::filesystem::exists(dir_path))
  {
    std::filesystem::create_directories(dir_path);
  }

  auto error_file_path = dir_path / "error_steps.txt";
  std::ofstream error_file(error_file_path, std::ios_base::app);

  error_file << timestep_number
    << "\t" << std::setw(12) << l2_error
    << "\t" << std::setw(12) << GridTools::minimal_cell_diameter(triangulation)
    << std::endl;
}

void ReferenceSolution_Prototype::output_final_error()
{
  auto error_file_path = output_path / "error.txt";
  std::ofstream error_file(error_file_path, std::ios_base::app);

  error_file << degree
    << "\t" << std::setw(12) << GridTools::minimal_cell_diameter(triangulation)
    << "\t" << std::setw(12) << GridTools::maximal_cell_diameter(triangulation)
    << "\t" << std::setw(12) << timestep_width
    << "\t" << std::setw(12) << max_l2_error
    << std::endl;
}

void ReferenceSolution_Prototype::output_matrices()
{

  const auto matrix_path = output_path / "matrices";
  std::filesystem::create_directories(matrix_path);

  // mass matrix
  {
    const auto out_file_path = matrix_path / "mass_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(mass, out_file);
  }

  // inv mass matrix
  {
    const auto out_file_path = matrix_path / "inv_mass_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(inv_mass, out_file);
  }

  // curl matrix
  {
    const auto out_file_path = matrix_path / "curl_matrix.dat";
    std::ofstream out_file(out_file_path);
    MaxwellProblem::Tools::output_matrix(curl, out_file);
  }
}

void ReferenceSolution_Prototype::print_mesh_info(std::ostream& stream)
{
  stream << "Mesh Info: \n"
    << std::string(20, '_') << std::endl
    << " minimal cell diameter: " << GridTools::minimal_cell_diameter(triangulation) << "\n"
    << " maximal cell diameter: " << GridTools::maximal_cell_diameter(triangulation) << "\n"
    << "       number of cells: " << triangulation.n_cells() << "\n"
    << "number of active cells: " << triangulation.n_active_cells() << "\n"
    << "number of active faces: " << triangulation.n_active_faces() << "\n"
    << "                  DoFs: " << dof_handler.n_dofs() << "\n"
    << std::string(20, '_') << std::endl;
}

void ReferenceSolution_Prototype::run()
{

  // j_current is zero
  j_current_modal = 0;
  j_current_nodal = 0;
  j_current_modal_space = 0;
  j_current_nodal_space = 0;

  // reset values
  l2_error = 0.0;
  max_l2_error = 0.0;
  time = start_time;
  int timestep_number = 0;

  solution_nodal = 0;
  solution_modal = 0;
  solution_difference = 0;

  if (!save_ref_sol) {
    // output intial error
    switch (mode)
    {
    case Nodal:
      l2_error = compute_error_L2(solution_nodal);
      break;
    case Modal:
      l2_error = compute_error_L2(solution_modal);
      break;
    case Difference:
      l2_error = compute_L2_norm(solution_difference);
      break;
    }
    max_l2_error = l2_error > max_l2_error ? l2_error : max_l2_error;
    output_error(timestep_number);
    std::cout << "Initial error:            " << l2_error << "\n";
  }
  else {
    switch (mode)
    {
    case Nodal:
      ref_sol.store(time, solution_nodal);
      break;
    case Modal:
      ref_sol.store(time, solution_modal);
      break;
    case Difference:
      std::cerr << "Ref sol for difference?\n";
      break;
    }
    //ref_sol.store(time, solution_modal);
  }
  if (vtu_output)
    output_step(timestep_number);

  // initialise time integrator
  // initialise time integrator
  MaxwellProblem::TimeIntegration::Leapfrog<SparseMatrix<double>, SparseMatrix<double>>
    integrator_nodal(
      inv_mass.block(0, 0),
      inv_mass.block(1, 1),
      curl.block(1, 0),
      curl.block(0, 1),
      timestep_width);
  
  MaxwellProblem::TimeIntegration::Leapfrog<SparseMatrix<double>, SparseMatrix<double>>
    integrator_modal(
      inv_mass.block(0, 0),
      inv_mass.block(1, 1),
      curl.block(1, 0),
      curl.block(0, 1),
      timestep_width);

  time += timestep_width;
  timestep_number++;

  dealii::Timer eta_timer;
  const int eta_steps = 100;
  eta_timer.start();

  const int error_steps = total_timesteps / error_ratio; // calculate error in every step

  // Nodal Interpolation Setup
  dealii::FEValuesExtractors::Vector E(1);
  dealii::ComponentMask E_mask = fe.component_mask(E);
  FunctionLift func_lift{surface_current};
  if (mode == Nodal || mode == Difference){
    dealii::VectorTools::interpolate(dof_handler, func_lift, j_current_nodal, E_mask);
    lift_matrix.vmult(j_current_nodal_space, j_current_nodal);
  }
  if (mode == Modal || mode == Difference ){
    // Modal Projection Setup
    lift_operator.assemble_modal_rhs(surface_current, j_current_modal_space);
  }

  for (; timestep_number <= total_timesteps; time += timestep_width, timestep_number++)
  {
    auto time_factor = time_modulation(time - timestep_width / 2);

    if (mode == Nodal || mode == Difference){
      // Nodal Interpolation
      j_current_nodal.block(1) = j_current_nodal_space.block(1);
      j_current_nodal.block(1) *= time_factor;
      integrator_nodal.integrate_step(solution_nodal.block(0), solution_nodal.block(1), j_current_nodal.block(1));
    }
    if (mode == Modal || mode == Difference ){
      // Modal Projection
      j_current_modal.block(1) = j_current_modal_space.block(1);
      j_current_modal.block(1) *= time_factor;
      integrator_modal.integrate_step(solution_modal.block(0), solution_modal.block(1), j_current_modal.block(1));
    }


    if (timestep_number % eta_steps == 0) {
      eta_timer.stop();
      auto eta_hours = ((eta_timer.wall_time() / eta_steps) * (total_timesteps - timestep_number)) / (60 * 60);
      std::cout << "timestep: "
                << timestep_number << "/" << total_timesteps
                << "\t eta: " << eta_hours * 60 << " min"
                << std::endl;
    }

    if (timestep_number % error_steps == 0 || timestep_number == static_cast<int>((end_time - start_time) * total_timesteps))
    {
      if (!save_ref_sol) {
        switch (mode)
        {
        case Nodal:
          l2_error = compute_error_L2(solution_nodal);
          break;
        case Modal:
          l2_error = compute_error_L2(solution_modal);
          break;
        case Difference:
          solution_difference = solution_nodal;
          solution_difference -= solution_modal;
          l2_error = compute_L2_norm(solution_difference);
          break;
        }
        max_l2_error = l2_error > max_l2_error ? l2_error : max_l2_error;

        std::cout << "timestep: "
          << timestep_number << "/" << total_timesteps
          << "\t error: " << l2_error
          << "\n"
          << std::flush;

        output_error(timestep_number);
      }
      else {
        switch (mode)
        {
        case Nodal:
          ref_sol.store(time, solution_nodal);
          break;
        case Modal:
          ref_sol.store(time, solution_modal);
          break;
        default:
          std::cerr << "Ref sol for difference?\n";
          break;
        }
        //ref_sol.store(time, solution_modal);

        std::cout << "timestep: "
          << timestep_number << "/" << total_timesteps;
        
        switch (mode)
        {
        case Nodal:
          std::cout << "\t l2: "  << solution_nodal.l2_norm() << "\n" << std::flush;
          break;
        case Modal:
          std::cout << "\t l2: "  << solution_modal.l2_norm() << "\n" << std::flush;
          break;
        case Difference:
          std::cout << "\t l2: "  << solution_difference.l2_norm() << "\n" << std::flush;
          break;
        }
      }

      if (vtu_output)
        output_step(timestep_number);
      eta_timer.restart();
    }
  }

  //if (vtu_output)
  //  output_step(timestep_number);
  if (!save_ref_sol)
    output_final_error();
}

int main(int argc, char** argv)
{

  if (argc != 2) {
    std::cerr << "Wrong arguments\n";
    return 1;
  }


  // init fftw
  fftwq_init_threads();
  fftwq_plan_with_nthreads(16);
  fftwq_make_planner_thread_safe();

  std::string config_file = argv[1];

  // boost property tree
  pt::ptree tree;
  pt::read_json(config_file, tree);

  const auto const_time_modulation = [](double) {return 1.;};

  const auto time_modulation = [](double time) {
    const auto sin_v = std::sin(2 * M_PI * time);
    return sin_v*sin_v;
  };

  //const auto time_modulation = [](double time) {
  //  
  //  return time * time * std::exp(2*time);
  //};

  //const auto time_modulation = [](double time) {
  //  return std::tanh(20 * time * time);
  //};

  const double start_time = tree.get<double>("experiment.start_time");
  const double end_time = tree.get<double>("experiment.end_time");


  BOOST_FOREACH(pt::ptree::value_type & v, tree.get_child("experiment.alphas")) {
    double alpha = boost::lexical_cast<double>(v.second.data());

    std::stringstream ss;
    ss << "alpha_" << boost::lexical_cast<std::string>(alpha);
    auto output_path_alpha = std::filesystem::current_path() / ss.str();
    if (!std::filesystem::exists(output_path_alpha))
    {
      std::filesystem::create_directories(output_path_alpha);
    }

    // generate fourier current
    FourierCurrent surface_current(18, alpha, const_time_modulation, FourierCurrent::SymmetricDirichletNew);
    {
      std::filesystem::path out_file = output_path_alpha / "fourier_current.dat";
      std::ofstream out(out_file);
      surface_current.output_numpy(out);
    }

    // calculate ref_sol
    ReferenceSolution<2, dealii::BlockVector<double>> ref_sol(output_path_alpha, 10e12);
    {
      int ref_deg = tree.get<int>("reference_solution.degree");
      int ref_refinements = tree.get<int>("reference_solution.edge_refinements");
      double ref_mesh_size = tree.get<double>("reference_solution.mesh_size");
      double ref_time_steps = tree.get<double>("reference_solution.time_steps");
      int ref_error_ratio = tree.get<int>("reference_solution.error_ratio");

      ReferenceSolution_Prototype prototype(ref_deg,
        ref_mesh_size,
        ref_refinements,
        ref_time_steps,
        start_time,
        end_time,
        ref_error_ratio,
        output_path_alpha,
        1,
        1,
        ModeSelection::Modal,
        time_modulation,
        ref_sol,
        true,
        surface_current);
      prototype.assemble();
      prototype.print_mesh_info(std::cout);
      prototype.set_vtu_output(true);
      prototype.run();
    }

    BOOST_FOREACH(pt::ptree::value_type & w, tree.get_child("experiment.degrees")) {
      int degree = boost::lexical_cast<int>(w.second.data());
      double time_steps = tree.get<double>("experiment.time_steps");
      int error_ratio = tree.get<double>("experiment.error_ratio");

      ss.str("");
      ss << "degree_" << boost::lexical_cast<std::string>(degree);

      auto output_path = output_path_alpha / ss.str();
      if (!std::filesystem::exists(output_path))
      {
        std::filesystem::create_directories(output_path);
      }

      // mesh size range
      double mesh_upper = tree.get<double>("experiment.mesh.mesh_upper");
      double mesh_lower = tree.get<double>("experiment.mesh.mesh_lower");
      int points = tree.get<int>("experiment.mesh.points");
      std::vector<double> mesh_range = MaxwellProblem::Tools::log_spaced(mesh_lower, mesh_upper, points);
      std::reverse(mesh_range.begin(), mesh_range.end());

      for (const auto& mesh_size : mesh_range) {
        ReferenceSolution_Prototype prototype(degree,
          mesh_size,
          0,
          time_steps,
          start_time,
          end_time,
          error_ratio,
          output_path,
          1,
          1,
          ModeSelection::Modal,
          time_modulation,
          ref_sol,
          false,
          surface_current);
        prototype.set_vtu_output(true);
        prototype.assemble();
        prototype.print_mesh_info(std::cout);
        prototype.run();
      }
    }

    //deinit fftw
    fftwq_cleanup_threads();
  }



  return 0;
}
