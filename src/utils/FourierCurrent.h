#ifndef FOURIER_CURRENT_H
#define FOURIER_CURRENT_H

#include <memory>
#include <type_traits>
#include <random>
#include <functional>
#include <iostream>

#include "deal.II/base/function.h"

#include "fftw3.h"

class FourierCurrent: public dealii::Function<2, double> {
public:
  enum Method {
    Random = 0,
    Dirichlet,
    SymmetricDirichlet,
    SymmetricDirichletNew,
    SymmetricDirichletFixed
  };

  FourierCurrent(
    unsigned int exponent,
    double alpha,
    std::function<double(double)> time_modulation,
    FourierCurrent::Method method = FourierCurrent::Random,
    double epsilon = 10e-6,
    double initial_time = 0.0);

  ~FourierCurrent();

  double value(const dealii::Point<2>& p, const unsigned int component = 0) const override;

  void output_numpy(std::ostream &out, const unsigned int precision = 9);

  int get_N() const {return N;};
  double get_normalization() const {return normalization;};

private:
  const unsigned int exponent;

  const double alpha;
  const std::function<double(double)> time_modulation;
  const FourierCurrent::Method method;
  const double epsilon;

  const int N;
  const int N_half;

  fftwq_complex *in;
  fftwq_complex *out;

  fftwq_plan plan;
  fftwq_plan back_plan;

  std::random_device rd;
  std::mt19937 gen;
  std::uniform_real_distribution<long double> dis;

  __float128 normalization;

  // index = -N/2,...,N/2 - 1 -> k = 0,...,N-1
  __float128 get_space_value_real(int index) const {
    auto sign = (index % 2 == 0) ? 1 : -1;//std::pow(-1, std::abs(index));
    return sign * out[static_cast<unsigned int>(index + N_half)][0] / normalization;
  };

  __float128 get_space_value_imag(int index) const {
    auto sign = std::pow(-1, std::abs(index));
    return sign * out[static_cast<unsigned int>(index + N_half)][1] / normalization;
  };

  void generate_random();
  void generate_dirichlet();
  void generate_symmetric_dirichlet();
  void generate_symmetric_dirichlet_new();
  void generate_symmetric_dirichlet_fixed();

  __float128 calculate_l2_norm();
  __float128 max_abs_imag_value();

};

#endif
