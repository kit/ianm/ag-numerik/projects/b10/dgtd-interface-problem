
#include "PointExcitation.h"

#include <cmath>
#include "math.h"

PointExcitation::PointExcitation(
    const double omega,
    const double t_abscissa,
    const double t_std,
    const double amp,
    const double x_abscissa,
    const double x_std,
    const double y_abscissa,
    const double y_std)
    : Function(2, 0.0),
      omega{omega},
      t_abscissa{t_abscissa},
      t_std{t_std},
      amp{amp},
      x_abscissa{x_abscissa},
      x_std{x_std},
      y_abscissa{y_abscissa},
      y_std{y_std} {};

double PointExcitation::value(const dealii::Point<2> &p, const unsigned int /*component*/) const
{

    // time calculation
    const auto time = this->get_time();

    const double time_value =
        std::sin(omega * time) * std::exp((-(time - t_abscissa) * (time - t_abscissa)) / (t_std * t_std));

    // space calculation

    const auto x = p[0];
    const auto y = p[1];

    const double x_value = std::exp((-(x - x_abscissa) * (x - x_abscissa)) / (x_std * x_std));
    const double y_value = std::exp((-(y - y_abscissa) * (y - y_abscissa)) / (y_std * y_std));

    const double space_value = amp * x_value * y_value;

    return time_value * space_value;
}