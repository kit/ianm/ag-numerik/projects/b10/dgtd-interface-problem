#ifndef POLYNOMIAL_SOLUTION_H_
#define POLYNOMIAL_SOLUTION_H_

#include <vector>
#include <functional>

#include "deal.II/base/function.h"


class PolynomialSolution : public dealii::Function<2, double>
{
private:
  // coefficients of polynomial in x1 direction
  // ordering: lowest degree coefficient first
  std::vector<double> polynomial_1_coefficients;
  // coefficients of polynomial in time 
  // ordering: lowest degree coefficient first
  const std::function<double(double)>& time_func;
  const std::function<double(double)>& derivative_time_func;
  const std::function<double(double)>& second_derivative_time_func;

  // degree of monomial in x2 direction
  const int degree_2;

  std::vector<double> polynomial_2_coefficients;


public:
  PolynomialSolution(
    std::vector<double> polynomial_1_coefficients,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  );

  double value(
    const dealii::Point<2> &p,
    const unsigned int component = 0) const override;
};

class PolynomialSolutionBulkCurrent : public dealii::Function<2, double>
{
private:
  // coefficients of polynomial in x1 direction
  // ordering: lowest degree coefficient first
  std::vector<double> polynomial_1_coefficients;
  // coefficients of polynomial in time 
  // ordering: lowest degree coefficient first
  const std::function<double(double)>& time_func;
  const std::function<double(double)>& derivative_time_func;
  const std::function<double(double)>& second_derivative_time_func;

  // degree of monomial in x2 direction
  const int degree_2;

  std::vector<double> polynomial_2_coefficients;



public:
  PolynomialSolutionBulkCurrent(
    std::vector<double> polynomial_1_coefficients,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  );

  double value(
    const dealii::Point<2> &p,
    const unsigned int component = 0) const override;
};

class PolynomialSolutionSurfaceCurrent : public dealii::Function<2, double>
{
private:
  // coefficients of polynomial in x1 direction
  // ordering: lowest degree coefficient first
  std::vector<double> polynomial_1_coefficients_left;
  std::vector<double> polynomial_1_coefficients_right;
  // coefficients of polynomial in time 
  // ordering: lowest degree coefficient first
  const std::function<double(double)>& time_func;
  const std::function<double(double)>& derivative_time_func;
  const std::function<double(double)>& second_derivative_time_func;

  // degree of monomial in x2 direction
  const int degree_2;

  std::vector<double> polynomial_2_coefficients;


public:
  PolynomialSolutionSurfaceCurrent(
    std::vector<double> polynomial_1_coefficients_left,
    std::vector<double> polynomial_1_coefficients_right,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  );

  double value(
    const dealii::Point<2> &p,
    const unsigned int component = 0) const override;
};


#endif //POLYNOMIAL_SOLUTION_H_