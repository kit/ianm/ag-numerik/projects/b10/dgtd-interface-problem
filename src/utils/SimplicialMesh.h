#ifndef SIMPLICIAL_MESH_H_
#define SIMPLICIAL_MESH_H_

#include <gmsh.h>

void generate_2D_domain(
    const double mesh_size,
    const double interface_mesh_size,
    const std::string& file_name);

void generate_2D_domain(
    const double mesh_size,
    const std::string& file_name);

#endif //SIMPLICIAL_MESH_H_