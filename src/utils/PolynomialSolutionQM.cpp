
#include "PolynomialSolutionQM.h"

#include "HornerScheme.h"

#include "boost/math/special_functions/binomial.hpp"

PolynomialSolution::PolynomialSolution(
    std::vector<double> polynomial_1_coefficients,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  ) : dealii::Function<2, double>(3),
polynomial_1_coefficients{ polynomial_1_coefficients },
time_func{ time_func },
derivative_time_func{derivative_time_func},
second_derivative_time_func{second_derivative_time_func},
degree_2{ degree_2 } {
	polynomial_2_coefficients.resize(2*degree_2 + 1);
	for (int j = 0; j < degree_2; ++j) {
		polynomial_2_coefficients[j] = 0;
	}
	for (int j = degree_2; j < 2*degree_2 + 1; ++j) {
		polynomial_2_coefficients[j] = std::pow(-1,j-degree_2) * 
			boost::math::binomial_coefficient<double>(degree_2, j - degree_2);
	}
}


double PolynomialSolution::value(
	const dealii::Point<2>& p,
	const unsigned int component) const
{
	const auto time = this->get_time();
	//std::cout << "time: " << time << "\n";
	const auto x_1 = p[0];
	const auto x_2 = p[1];

	const auto poly_x1 = 
		MaxwellProblem::Tools::horner_scheme(polynomial_1_coefficients, x_1);

	switch (component)
	{
	case 0:	// H_3 component
		{
			const auto poly_time = time_func(time);
			const auto derivative_poly_x2 = 
				MaxwellProblem::Tools::horner_scheme_derivative(polynomial_2_coefficients, x_2);
			//std::cout << "poly_time: " << poly_time << "\n";
			return poly_time * poly_x1 * derivative_poly_x2;
		}
		break;
	case 1:	// E_1 component
		{
			const auto derivative_poly_time = derivative_time_func(time);
			const auto poly_x2 = 
				MaxwellProblem::Tools::horner_scheme(polynomial_2_coefficients, x_2);

			return derivative_poly_time * poly_x1 * poly_x2;
		}
		break;
	case 2:	// E_2 component
		{
			return 0.0;
		}
		break;
	default:
		throw dealii::ExcMessage("Polynomial Solution only has 3 components.");
	}
}

PolynomialSolutionBulkCurrent::PolynomialSolutionBulkCurrent(
    std::vector<double> polynomial_1_coefficients,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  ) : dealii::Function<2, double>(2),
polynomial_1_coefficients{ polynomial_1_coefficients },
time_func{time_func},
derivative_time_func{derivative_time_func},
second_derivative_time_func{second_derivative_time_func},
degree_2{ degree_2 } {
	polynomial_2_coefficients.resize(2*degree_2 + 1);
	for (int j = 0; j < degree_2; ++j) {
		polynomial_2_coefficients[j] = 0;
		std::cout << polynomial_2_coefficients[j] << " ____ ";
	}
	for (int j = degree_2; j < 2*degree_2 + 1; ++j) {
		polynomial_2_coefficients[j] = std::pow(-1,j-degree_2) * 
			boost::math::binomial_coefficient<double>(degree_2, j - degree_2);
		std::cout << polynomial_2_coefficients[j] << " ____ ";
	}
	std::cout << "\n";
}

double PolynomialSolutionBulkCurrent::value(
	const dealii::Point<2>& p,
	const unsigned int component) const
{
	const auto time = this->get_time();

	const auto x_1 = p[0];
	const auto x_2 = p[1];

	const auto poly_time = time_func(time);

	switch (component)
	{
	case 0:	// E_1 component
		{
			const auto poly_x1 =
				MaxwellProblem::Tools::horner_scheme(polynomial_1_coefficients, x_1);
			const auto poly_x2 = 
				MaxwellProblem::Tools::horner_scheme(polynomial_2_coefficients, x_2);
			const auto second_derivative_poly_x2 =
				MaxwellProblem::Tools::horner_scheme_second_derivative(polynomial_2_coefficients, x_2);
			const auto second_derivative_time = second_derivative_time_func(time);
			return poly_time * poly_x1 * second_derivative_poly_x2 
				-second_derivative_time * poly_x1 * poly_x2;
			
		}
		break;
	case 1:	// E_2 component
		{
			const auto derivative_poly_x1 =
				MaxwellProblem::Tools::horner_scheme_derivative(polynomial_1_coefficients, x_1);
			const auto derivative_poly_x2 = 
				MaxwellProblem::Tools::horner_scheme_derivative(polynomial_2_coefficients, x_2);

			return -poly_time * derivative_poly_x1 * derivative_poly_x2;
		}
		break;
	default:
		throw dealii::ExcMessage("Polynomial Solution Current only has 2 components.");
	}
}

PolynomialSolutionSurfaceCurrent::PolynomialSolutionSurfaceCurrent(
    std::vector<double> polynomial_1_coefficients_left,
    std::vector<double> polynomial_1_coefficients_right,
    const std::function<double(double)>&  time_func,
    const std::function<double(double)>&  derivative_time_func,
    const std::function<double(double)>&  second_derivative_time_func,
    const int degree_2
  ) : dealii::Function<2, double>(2),
polynomial_1_coefficients_left{ polynomial_1_coefficients_left },
polynomial_1_coefficients_right{ polynomial_1_coefficients_right },
time_func{time_func},
derivative_time_func{derivative_time_func},
second_derivative_time_func{second_derivative_time_func},
degree_2{ degree_2 } {
	polynomial_2_coefficients.resize(2*degree_2 + 1);
	for (int j = 0; j < degree_2; ++j) {
		polynomial_2_coefficients[j] = 0;
	}
	for (int j = degree_2; j < 2*degree_2 + 1; ++j) {
		polynomial_2_coefficients[j] = std::pow(-1,j-degree_2) * 
			boost::math::binomial_coefficient<double>(degree_2, j - degree_2);
	}
}

double PolynomialSolutionSurfaceCurrent::value(
	const dealii::Point<2>& p,
	const unsigned int component) const {

		if (component == 0) return 0.0;

		const auto time = this->get_time();
		const auto poly_time = time_func(time);
		
		const auto poly_x1_left = 
			MaxwellProblem::Tools::horner_scheme(polynomial_1_coefficients_left, 1.0);
		const auto poly_x1_right = 
			MaxwellProblem::Tools::horner_scheme(polynomial_1_coefficients_right, 1.0);
		const auto poly_x1_jump = poly_x1_right - poly_x1_left;

		const auto x_2 = p[1];
		const auto derivative_poly_x2 = 
			MaxwellProblem::Tools::horner_scheme_derivative(polynomial_2_coefficients, x_2);

		return poly_time * poly_x1_jump * derivative_poly_x2;
}