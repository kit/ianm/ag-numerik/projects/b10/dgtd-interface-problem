#ifndef SINUS_CURRENT_H_
#define SINUS_CURRENT_H_

#include <cmath>
#include <math.h>

#include "deal.II/base/function.h"

class SinusCurrent : public dealii::Function<2, double>
{
private:
    const unsigned int degree;
    const unsigned int mode;

public:
    SinusCurrent(const unsigned int degree, const unsigned int mode);
    double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;
};

#endif // SINUS_CURRENT_H_