
#include "SimplicialMesh.h"

#include <thread>

void generate_2D_domain(
    const double mesh_size,
    const double interface_mesh_size,
    const std::string& file_name) 
{
    gmsh::initialize();
    gmsh::option::setNumber("General.NumThreads", std::thread::hardware_concurrency());

    gmsh::model::add(file_name);

    auto p1 = gmsh::model::geo::addPoint(0,0,0, mesh_size);
    auto p2 = gmsh::model::geo::addPoint(1,0,0, interface_mesh_size);
    auto p3 = gmsh::model::geo::addPoint(1,1,0, interface_mesh_size);
    auto p4 = gmsh::model::geo::addPoint(0,1,0, mesh_size);
    auto p5 = gmsh::model::geo::addPoint(2,0,0, mesh_size);
    auto p6 = gmsh::model::geo::addPoint(2,1,0, mesh_size);


    auto l1 = gmsh::model::geo::addLine(p1, p2);
    auto l2 = gmsh::model::geo::addLine(p2, p3);
    auto l3 = gmsh::model::geo::addLine(p3, p4);
    auto l4 = gmsh::model::geo::addLine(p4, p1);
    auto l5 = gmsh::model::geo::addLine(p3, p6);
    auto l6 = gmsh::model::geo::addLine(p6, p5);
    auto l7 = gmsh::model::geo::addLine(p5, p2);


    auto cl1 = gmsh::model::geo::addCurveLoop({l1,l2,l3,l4});
    auto cl2 = gmsh::model::geo::addCurveLoop({l2,l5,l6,l7});

    auto s1 = gmsh::model::geo::addPlaneSurface({cl1});
    auto s2 = gmsh::model::geo::addPlaneSurface({cl2});

    gmsh::model::geo::synchronize();

    gmsh::model::addPhysicalGroup(2, {s1}, 0);
    gmsh::model::addPhysicalGroup(2, {s2}, 1);
    gmsh::model::setPhysicalName(2, 0, "MaterialID: 0");
    gmsh::model::setPhysicalName(2, 1, "MaterialID: 1");

    gmsh::model::geo::synchronize();
    gmsh::model::mesh::generate(2);
    gmsh::model::mesh::optimize("");
    gmsh::write(file_name);

    gmsh::finalize();
}

void generate_2D_domain(
    const double mesh_size,
    const std::string& file_name)
{
    generate_2D_domain(mesh_size, mesh_size, file_name);
}
