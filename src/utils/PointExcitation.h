#ifndef POINT_EXCITATION_H_
#define POINT_EXCITATION_H_

#include "deal.II/base/function.h"

class PointExcitation : public dealii::Function<2, double>
{
private:
    // Time coefficients
    const double omega;         // phase of sinus 
    const double t_abscissa;    // center of gaussian
    const double t_std;         // std of gaussian

    // Space coefficients
    const double amp;           // space amplitude -> could vary for different
    const double x_abscissa;    // center of gaussian in x
    const double x_std;         // std of gaussian in x
    const double y_abscissa;    // center of gaussian in y
    const double y_std;         // std of gaussian in y    

public:
    PointExcitation(
        const double omega,
        const double t_abscissa,
        const double t_std,
        const double amp,
        const double x_abscissa,
        const double x_std,
        const double y_abscissa,
        const double y_std);
    
    double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;
};

#endif // POINT_EXCITATION_H_