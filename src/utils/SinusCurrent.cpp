
#include "SinusCurrent.h"

SinusCurrent::SinusCurrent(const unsigned int degree, const unsigned int mode)
    : degree{degree},
      mode{mode} {};

double SinusCurrent::value(const dealii::Point<2> &p, const unsigned int component) const
{
    if (component == 0 || std::abs(p[0] - 1) > 1e-9)
        return 0;

    const auto x = p[1];
    return (std::pow(2, 2 * degree) 
        * std::pow(x, degree) 
        * std::pow((1 - x), degree) 
        * std::pow(std::sin(M_PI * mode * x) , 15));
}