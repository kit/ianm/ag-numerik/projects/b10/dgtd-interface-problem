#ifndef REFERENCESOLUTION_H_
#define REFERENCESOLUTION_H_

#include <filesystem>
#include <functional>
#include <set>

#include "deal.II/dofs/dof_handler.h"
#include "deal.II/grid/tria.h"
#include "deal.II/fe/fe.h"
#include "deal.II/numerics/fe_field_function.h"

template <int dim, typename VectorType>
class ReferenceSolution {
private:
  dealii::Triangulation<dim> ref_triangulation;

  dealii::DoFHandler<dim> ref_dof_handler;
  const std::filesystem::path ref_path;

  const size_t precision;
  VectorType vector_tmp;
  std::set<size_t> time_stamp_set;

  bool initialized = false;

  inline size_t cut_off(double time_stamp) {
    return static_cast<size_t>(time_stamp * precision);
  };

  inline double get_time_back(size_t cut_time_stamp) {
    return cut_time_stamp / static_cast<double>(precision);
  };

  /**
  * @brief Load a solution vector.
  *
  * This function loads the solution
  * vector for a given time stamp.
  *
  * Please take care of the precision.
  *
  * The function will fail if the time
  * stamp is not present.
  *
  * @param time_stamp
  * @param vector
  */
  void load(size_t cut_time_stamp, VectorType& vector);

public:

  /**
   * @brief Constructor reference solution
   *
   * The precision tells how many decimal places
   * of a double valued time stamp are used.
   * For example, with precision = 10e2 the
   * two time stamps 1.01 and 1.011 are considered
   * equal.
   *
   * @param output Path data gets stored at.
   * @param precision
   */
  ReferenceSolution(
    const std::filesystem::path& output,
    const size_t precision = 1e10);

  /**
   * @brief Constructor reference solution
   *
   * Same as above but it will create a
   * temporary folder /tmp/ref_sol
   * were the data gets stored.
   *
   * @param precision
   */
  ReferenceSolution(
    const size_t precision = 1e10);

  /**
   * @brief Initialize reference solution.
   *
   * This function needs to be called before
   * the reference solution is called.
   *
   * Note that this function will copy the triangulation
   * and it is no problem if the old reference gets deleted.
   *
   * Furthermore, since the DoFHandler cannot be copied,
   * a new one needs to be constructed.
   * For that reason, one needs the provide a
   * dof setup function.
   * Look up the examples for the details.
   *
   * @param triangulation
   * @param dof_setup_func
   */
  void initialize(
    dealii::Triangulation<dim>& triangulation,
    std::function<void(dealii::DoFHandler<dim>&)> dof_setup_func);

  /**
   * @brief Store a solution vector.
   *
   * This function stores a solution vector with
   * a corresponding time stamp.
   *
   * Please take care of the precision.
   *
   * The function will fail if the time stamp
   * is already present.
   *
   * @param time_stamp
   * @param vector
   */
  void store(double time_stamp, VectorType &vector);

  /**
   * @brief Interpolate between solution vector.
   *
   * This function will interpolate the solution
   * vector at the given time stamp between
   * to vectors stored.
   *
   * The function will return the solution
   * vector if the time stamp is present.
   *
   * The function will fail if the time stamp
   * is out of bounds.
   *
   * @param time_stamp
   * @param vector
   */
  void interpolate(double time_stamp, VectorType& vector);


  double calculate_error(
    double time,
    dealii::Functions::FEFieldFunction<dim, VectorType> &func,
    dealii::VectorTools::NormType norm_type,
    int quad_order);

  double calculate_error_local_dof(
    double time,
    dealii::BlockVector<double> &solution,
    dealii::DoFHandler<dim> &local_dof_handler,
    dealii::VectorTools::NormType norm_type,
    int quad_order);

};

#endif //REFERENCESOLUTION_H_
