#ifndef REFERENCESOLUTION_HH_
#define REFERENCESOLUTION_HH_

#include "ReferenceSolution.h"

#include <fstream>

#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/base/quadrature.h>

template<int dim, typename VectorType>
ReferenceSolution<dim, VectorType>::ReferenceSolution(
  const std::filesystem::path& output,
  const size_t precision):
  ref_dof_handler(ref_triangulation),
  ref_path(output / "ref_sol"),
  precision(precision) {

  if (!std::filesystem::exists(ref_path)) {
    std::filesystem::create_directory(ref_path);
  }
}

template<int dim, typename VectorType>
ReferenceSolution<dim, VectorType>::ReferenceSolution(
  const size_t precision
):
  ReferenceSolution(
    std::filesystem::temp_directory_path(),
    precision) {}

template<int dim, typename VectorType>
void ReferenceSolution<dim, VectorType>::initialize(
  dealii::Triangulation<dim>& triangulation,
  std::function<void(dealii::DoFHandler<dim>&)> dof_setup_func) {

  // copy triangulation
  ref_triangulation.copy_triangulation(triangulation);
  // setup dof handler
  dof_setup_func(ref_dof_handler);

  // reference solution is now usable
  initialized = true;
}

template<int dim, typename VectorType>
void ReferenceSolution<dim, VectorType>::store(double time_stamp, VectorType &vector) {

  vector_tmp.reinit(vector);

  if (!initialized)
    throw dealii::ExcInternalError("Reference solution not initialized.");

  auto cut_time_stamp = cut_off(time_stamp);

  if (auto search = time_stamp_set.find(cut_time_stamp); search != time_stamp_set.end())
    throw dealii::ExcInternalError("Time stamp already present.");
  
  time_stamp_set.emplace(cut_time_stamp);

  std::string file_name = "ref_sol_" + std::to_string(cut_time_stamp);
  auto file_path = ref_path / file_name;
  std::ofstream output_stream(file_path);
  vector.block_write(output_stream);
}

template<int dim, typename VectorType>
void ReferenceSolution<dim, VectorType>::load(size_t cut_time_stamp, VectorType& vector) {

  if (!initialized)
    throw dealii::ExcInternalError("Reference solution not initialized.");

  if (auto search = time_stamp_set.find(cut_time_stamp); search == time_stamp_set.end())
    throw dealii::ExcInternalError("Time stamp not present.");

  std::string file_name = "ref_sol_" + std::to_string(cut_time_stamp);
  auto file_path = ref_path / file_name;
  std::ifstream input_stream(file_path);
  vector.block_read(input_stream);
}

template<int dim, typename VectorType>
void ReferenceSolution<dim, VectorType>::interpolate(double time_stamp, VectorType& vector) {

  if (!initialized)
    throw dealii::ExcInternalError("Reference solution not initialized.");

  auto cut_time_stamp = cut_off(time_stamp);

  auto lower = time_stamp_set.lower_bound(cut_time_stamp);
  auto upper = time_stamp_set.upper_bound(cut_time_stamp);

  if (lower == time_stamp_set.begin() && upper == time_stamp_set.begin()) {
    // Asked for a time step below the first one
    // -> Return the first one
    // This really happens only because of round of errors

    double first_time_stamp = *(lower);
    return load(first_time_stamp, vector);
  }
  if (lower == time_stamp_set.end() && upper == time_stamp_set.end()) {
    // Asked for a time step above the last one
    // -> Return the last one
    // This really happens only because of round of errors

    double last_time_stamp = *(--upper);
    return load(last_time_stamp, vector);
  }
  else if (lower == upper && upper != time_stamp_set.end()) {
    // interpolate here!
    auto lower_time_stamp = *(--lower);
    auto upper_time_stamp = *(upper);

    VectorType tmp_vector;
    tmp_vector.reinit(vector);
    load(lower_time_stamp, vector);
    load(upper_time_stamp, tmp_vector);

    double lower_time = get_time_back(lower_time_stamp);
    double upper_time = get_time_back(upper_time_stamp);

    double theta = (time_stamp - lower_time) / (upper_time - lower_time);

    vector.sadd((1 - theta), theta, tmp_vector);
    return;
  }
  else {
    // time stamp is present
    return load(cut_time_stamp, vector);
  }
}

//template<int dim, typename VectorType>
//dealii::Functions::FEFieldFunction<dim, VectorType> 
//ReferenceSolution<dim, VectorType>::get_function(double time_stamp) {
//  if (!initialized)
//    throw dealii::ExcInternalError("Reference solution is not initialized.");
//
//  interpolate(time_stamp, vector_tmp_1);
//
//  return dealii::Functions::FEFieldFunction<dim, VectorType>(ref_dof_handler, vector_tmp_1);
//}

template<int dim, typename VectorType>
double ReferenceSolution<dim, VectorType>::calculate_error(
  double time,
  dealii::Functions::FEFieldFunction<dim, VectorType> &func,
  dealii::VectorTools::NormType norm_type,
  int quad_order) {

  if (!initialized)
    throw dealii::ExcInternalError("Reference solution is not initialized.");

  interpolate(time, vector_tmp);

  dealii::Vector<double> local_errors(ref_triangulation.n_active_cells());
  const dealii::QGauss<dim> quad(quad_order);
  //const QTrapezoid<1>  q_trapez;
  //const QIterated<dim> quad(q_trapez, quad_order);

  dealii::VectorTools::integrate_difference(
    dealii::MappingQ1<dim>{},
    ref_dof_handler,
    vector_tmp,
    func,
    local_errors,
    quad,
    norm_type);

  return dealii::VectorTools::compute_global_error(
    ref_triangulation,
    local_errors,
    norm_type);
}

template<int dim, typename VectorType>
double ReferenceSolution<dim, VectorType>::calculate_error_local_dof(
    double time,
    dealii::BlockVector<double> &solution,
    dealii::DoFHandler<dim> &local_dof_handler,
    dealii::VectorTools::NormType norm_type,
    int quad_order) {

  if (!initialized)
    throw dealii::ExcInternalError("Reference solution is not initialized.");

  interpolate(time, vector_tmp);

  dealii::Functions::FEFieldFunction<dim, VectorType>
     ref_func(ref_dof_handler, vector_tmp);

  dealii::Vector<double> local_errors(local_dof_handler.get_triangulation().n_active_cells());
  const dealii::QGaussLobatto<dim> quad(quad_order);
  //const QTrapezoid<1>  q_trapez;
  //const QIterated<dim> quad(q_trapez, quad_order);

  dealii::VectorTools::integrate_difference(
    dealii::MappingQ1<dim>{},
    local_dof_handler,
    solution,
    ref_func,
    local_errors,
    quad,
    norm_type);

  return dealii::VectorTools::compute_global_error(
    local_dof_handler.get_triangulation(),
    local_errors,
    norm_type);
}



#endif //REFERENCESOLUTION_HH_