
#include "LiftOperator.h"

#include <vector>

#include "deal.II/dofs/dof_tools.h"

LiftOperator::LiftOperator(dealii::FESystem<2>& fe,
  const dealii::MappingQ1<2>& mapping,
  const dealii::Quadrature<1>& face_quadrature,
  dealii::DoFHandler<2>& dof_handler,
  dealii::Function<2>& mu_function,
  dealii::Function<2>& eps_function)
  : fe{ fe },
  mapping{ mapping },
  face_quadrature{ face_quadrature },
  dof_handler{ dof_handler },
  fe_v_face{ fe, face_quadrature,
      dealii::UpdateFlags::update_quadrature_points
      | dealii::UpdateFlags::update_JxW_values
      | dealii::UpdateFlags::update_values },
  fe_v_face_neighbor{ fe, face_quadrature,
      dealii::UpdateFlags::update_values },
  mu_function{ mu_function },
  eps_function{ eps_function } {};

void LiftOperator::assemble_modal_rhs(dealii::Function<2>& func,
  dealii::BlockVector<double>& dst) {

  dst = 0;
  const dealii::FEValuesExtractors::Scalar H(0);
  const dealii::FEValuesExtractors::Vector E(1);

  auto dofs_per_cell = fe_v_face.dofs_per_cell;
  dealii::Vector<double> cell_vector(dofs_per_cell);
  dealii::Vector<double> cell_vector_ext(dofs_per_cell);
  std::vector<dealii::types::global_dof_index> dofs(dofs_per_cell);
  std::vector<dealii::types::global_dof_index> dofs_neighbor(dofs_per_cell);

  auto cell = dof_handler.begin_active();
  auto endc = dof_handler.end();

  for (; cell != endc; ++cell)
  {
    cell_vector = 0;
    cell_vector_ext = 0;
    int interfaces_per_cell = 0;

    if (cell->material_id() != 0)
    {
      continue;
    }

    for (unsigned int face_no = 0; face_no < dealii::GeometryInfo<2>::faces_per_cell; ++face_no)
    {
      auto face = cell->face(face_no);

      // check if face is on boundary
      if (face->at_boundary()) continue;

      auto neighbor = cell->neighbor(face_no);

      // check if face is on interface
      if (neighbor->material_id() == cell->material_id()) continue;

      interfaces_per_cell++;

      // Assert(!cell->neighbor_is_coarser(face_no), "Neighbor is coarser!");

      auto eps_value = eps_function.value(cell->center(), 0);
      auto mu_value = mu_function.value(cell->center(), 0);
      auto eps_neighbor_value = eps_function.value(neighbor->center(), 0);
      auto mu_neighbor_value = mu_function.value(neighbor->center(), 0);

      double w = std::sqrt(eps_value / mu_value);
      double w_ext = std::sqrt(eps_neighbor_value / mu_neighbor_value);
      double w_ges = w + w_ext;
      w /= w_ges;
      w_ext /= w_ges;

      auto face_no_neighbor = cell->neighbor_of_neighbor(face_no);
      fe_v_face.reinit(cell, face_no);
      fe_v_face_neighbor.reinit(neighbor, face_no_neighbor);

      auto n_quad_points = fe_v_face.n_quadrature_points;
      auto JxW = fe_v_face.get_JxW_values();

      // get function values
      std::vector<double> rhs_values(n_quad_points, 0.0);
      func.value_list(fe_v_face.get_quadrature_points(), rhs_values, 1);

      for (unsigned int point = 0; point < n_quad_points; ++point)
      {
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
        {

          // this is dirty but is there a better way?
          if (fe_v_face.get_fe().has_support_on_face(i, face_no))
          {
            const auto E_i = fe_v_face[E].value(i, point);

            auto E_rhs = w * E_i;

            //std::cout << rhs_values[point][1] << "\n";
            cell_vector(i) -= (rhs_values[point] * E_rhs[1]) * JxW[point];
          }

          // this is dirty but is there a better way?
          if (fe_v_face_neighbor.get_fe().has_support_on_face(i, face_no_neighbor))
          {
            const auto E_i_neighbor = fe_v_face_neighbor[E].value(i, point);

            auto E_rhs = w_ext * E_i_neighbor;

            cell_vector_ext(i) -= (rhs_values[point] * E_rhs[1]) * JxW[point];
          }
        }
      }

      cell->get_dof_indices(dofs);
      neighbor->get_dof_indices(dofs_neighbor);

      for (unsigned int i = 0; i < dofs_per_cell; ++i)
      {
        dst(dofs[i]) += cell_vector(i);
        dst(dofs_neighbor[i]) += cell_vector_ext(i);
      }
    }
    if (interfaces_per_cell > 1)
    {
      std::cout << "Cell has two interfaces on Gamma?\n";
    }
  }
}

void LiftOperator::generate_lift_matrix_pattern(
  dealii::BlockSparseMatrix<double>& lift_matrix,
  dealii::BlockSparsityPattern& lift_pattern)
{

  std::vector<dealii::types::global_dof_index> dofs_per_block =
    dealii::DoFTools::count_dofs_per_fe_block(dof_handler, { 0, 1 });

  const unsigned int n_H = dofs_per_block[0];
  const unsigned int n_E = dofs_per_block[1];

  dealii::BlockDynamicSparsityPattern dsp(2, 2);

  dsp.block(0, 0).reinit(n_H, n_H);
  dsp.block(1, 0).reinit(n_E, n_H);
  dsp.block(0, 1).reinit(n_H, n_E);
  dsp.block(1, 1).reinit(n_E, n_E);
  dsp.collect_sizes();

  const auto fe_components = fe.n_components();
  dealii::Table<2, dealii::DoFTools::Coupling> cell_coupling(
    fe_components, fe_components);
  dealii::Table<2, dealii::DoFTools::Coupling> face_coupling(
    fe_components, fe_components);

  for (auto const& i : { 0 }) {
    // coupling of H and H components
    cell_coupling[i][i] = dealii::DoFTools::none;
    face_coupling[i][i] = dealii::DoFTools::none;
    for (auto const& j : { 1, 2 }) {
      // coupling of E and E components
      cell_coupling[j][j] = dealii::DoFTools::none;
      face_coupling[j][j] = dealii::DoFTools::nonzero;
      // coupling of H and E
      cell_coupling[i][j] = dealii::DoFTools::none;
      face_coupling[i][j] = dealii::DoFTools::none;
      // coupling of E and H
      cell_coupling[j][i] = dealii::DoFTools::none;
      face_coupling[j][i] = dealii::DoFTools::none;
    }
  }

  dealii::DoFTools::make_flux_sparsity_pattern(dof_handler, dsp, cell_coupling, face_coupling);
  lift_pattern.copy_from(dsp);
  lift_matrix.reinit(lift_pattern);
}

void LiftOperator::assemble_lift_matrix(dealii::BlockSparseMatrix<double> &lift_matrix)
{
  
  const dealii::FEValuesExtractors::Scalar H(0);
  const dealii::FEValuesExtractors::Vector E(1);

  auto dofs_per_cell = fe_v_face.dofs_per_cell;
  dealii::FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);
  dealii::FullMatrix<double> cell_matrix_ext(dofs_per_cell, dofs_per_cell);
  std::vector<dealii::types::global_dof_index> dofs(dofs_per_cell);
  std::vector<dealii::types::global_dof_index> dofs_neighbor(dofs_per_cell);

  auto cell = dof_handler.begin_active();
  auto endc = dof_handler.end();

  for (; cell != endc; ++cell)
  {
    cell_matrix = 0;
    cell_matrix_ext = 0;
    int interfaces_per_cell = 0;

    if (cell->material_id() != 0)
    {
      continue;
    }

    for (unsigned int face_no = 0; face_no < dealii::GeometryInfo<2>::faces_per_cell; ++face_no)
    {
      auto face = cell->face(face_no);

      // check if face is on boundary
      if (face->at_boundary()) continue;

      auto neighbor = cell->neighbor(face_no);

      // check if face is on interface
      if (neighbor->material_id() == cell->material_id()) continue;

      interfaces_per_cell++;

      // Assert(!cell->neighbor_is_coarser(face_no), "Neighbor is coarser!");

      auto eps_value = eps_function.value(cell->center(), 0);
      auto mu_value = mu_function.value(cell->center(), 0);
      auto eps_neighbor_value = eps_function.value(neighbor->center(), 0);
      auto mu_neighbor_value = mu_function.value(neighbor->center(), 0);

      double w = std::sqrt(eps_value / mu_value);
      double w_ext = std::sqrt(eps_neighbor_value / mu_neighbor_value);
      double w_ges = w + w_ext;
      w /= w_ges;
      w_ext /= w_ges;

      auto face_no_neighbor = cell->neighbor_of_neighbor(face_no);
      fe_v_face.reinit(cell, face_no);
      fe_v_face_neighbor.reinit(neighbor, face_no_neighbor);

      auto n_quad_points = fe_v_face.n_quadrature_points;
      auto JxW = fe_v_face.get_JxW_values();

      for (unsigned int point = 0; point < n_quad_points; ++point)
      {
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
        {
          for (unsigned int j = 0; j < dofs_per_cell; ++j)
          {
            // this is dirty but is there a better way?
            if (fe_v_face.get_fe().has_support_on_face(i, face_no)
              && fe_v_face.get_fe().has_support_on_face(j, face_no))
            {
              const auto E_i = fe_v_face[E].value(i, point);
              const auto E_j = fe_v_face[E].value(j, point);

              auto E_rhs = w * E_i;

              //std::cout << rhs_values[point][1] << "\n";
              cell_matrix(i,j) -= (E_j * E_rhs) * JxW[point];
            }

            // this is dirty but is there a better way?
            if (fe_v_face_neighbor.get_fe().has_support_on_face(i, face_no_neighbor)
              && fe_v_face_neighbor.get_fe().has_support_on_face(j, face_no_neighbor))
            {
              const auto E_i_neighbor = fe_v_face_neighbor[E].value(i, point);
              const auto E_j_neighbor = fe_v_face_neighbor[E].value(j, point);

              auto E_rhs = w_ext * E_i_neighbor;

              cell_matrix_ext(i,j) -= (E_j_neighbor * E_rhs) * JxW[point];
            }
          }

          }
      }

      cell->get_dof_indices(dofs);
      neighbor->get_dof_indices(dofs_neighbor);

      for (unsigned int i = 0; i < dofs_per_cell; ++i)
      {
        for (unsigned int j = 0; j < dofs_per_cell; ++j)
        {
          lift_matrix.add(dofs[i], dofs[j], cell_matrix(i,j));
          lift_matrix.add(dofs_neighbor[i], dofs_neighbor[j], cell_matrix_ext(i,j));
        }
      }
    }
    if (interfaces_per_cell > 1)
    {
      std::cout << "Cell has two interfaces on Gamma?\n";
    }
  }
}