#ifndef LIFT_OPERATOR_H_
#define LIFT_OPERATOR_H_

#include "deal.II/base/function.h"

#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping_q1.h>
#include "deal.II/fe/fe_values.h"
#include "deal.II/fe/fe_values_extractors.h"
#include "deal.II/dofs/dof_handler.h"

#include "deal.II/lac/block_vector.h"
#include "deal.II/lac/block_sparsity_pattern.h"
#include "deal.II/lac/block_sparse_matrix.h"

class LiftOperator
{
private:

    dealii::FESystem<2> &fe;
    const dealii::MappingQ1<2> &mapping;
    const dealii::Quadrature<1> &face_quadrature;
    dealii::DoFHandler<2> &dof_handler;

    dealii::FEFaceValues<2> fe_v_face;
    dealii::FEFaceValues<2> fe_v_face_neighbor;

    const dealii::Function<2> &mu_function;
    const dealii::Function<2> &eps_function;


public:

    LiftOperator(
        dealii::FESystem<2> &fe,
        const dealii::MappingQ1<2> &mapping,
        const dealii::Quadrature<1> &face_quadrature,
        dealii::DoFHandler<2> &dof_handler,
        dealii::Function<2> &mu_function,
        dealii::Function<2> &eps_function);

    void assemble_modal_rhs(
        dealii::Function<2>& func,
        dealii::BlockVector<double>& dst);

    void generate_lift_matrix_pattern(
        dealii::BlockSparseMatrix<double> &lift_matrix,
        dealii::BlockSparsityPattern &lift_pattern);

    void assemble_lift_matrix(
        dealii::BlockSparseMatrix<double> &lift_matrix);
};



#endif //LIFT_OPERATOR_H_