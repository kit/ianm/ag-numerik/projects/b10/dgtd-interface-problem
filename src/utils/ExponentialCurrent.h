#ifndef EXP_CURRENT_H_
#define EXP_CURRENT_H_

#include <cmath>
#include <math.h>

#include "deal.II/base/function.h"

class ExponentialCurrent : public dealii::Function<2, double>
{
private:
    const unsigned int degree;
    const double sigma;

public:
    ExponentialCurrent(const unsigned int degree, const double sigma);
    double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;
};

#endif // EXP_CURRENT_H_