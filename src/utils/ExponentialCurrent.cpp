
#include "ExponentialCurrent.h"

ExponentialCurrent::ExponentialCurrent(const unsigned int degree, const double sigma)
    : degree{degree},
      sigma{sigma} {};

double ExponentialCurrent::value(const dealii::Point<2> &p, const unsigned int component) const
{
    if (component == 0 || std::abs(p[0] - 1) > 1e-9)
        return 0;

    const auto x = p[1];
    return (std::pow(2, 2 * degree) 
        * std::pow(x, degree) 
        * std::pow((1 - x), degree) 
        * std::exp( - (x-0.5)*(x-0.5)/sigma));
}