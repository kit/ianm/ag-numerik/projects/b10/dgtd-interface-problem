#ifndef CHEBYSHEV_CURRENT_H_
#define CHEBYSHEV_CURRENT_H_

#include <vector>

#include "deal.II/base/function.h"

template <int deg>
struct Chebyshev
{
  static double eval(double x)
  {
    // Recursively calculate using the recurrence relation: Tₖ(x) = 2 * x * Tₖ₋₁(x) - Tₖ₋₂(x)
    return 2.0 * x * Chebyshev<deg - 1>::eval(x) - Chebyshev<deg - 2>::eval(x);
  }
};

template <>
struct Chebyshev<0>
{
  static double eval(double /*x*/) { return 1.0; };
};

template <>
struct Chebyshev<1>
{
  static double eval(double x) { return x; };
};

template <int deg>
class ChebyshevCurrent : public dealii::Function<2, double>
{
  double value(const dealii::Point<2> &p, const unsigned int component = 0) const override;
};


template <int deg>
double ChebyshevCurrent<deg>::value(
    const dealii::Point<2> &p,
    const unsigned int component) const
{

  if (component == 0 || std::abs(p[0] - 1) > 1e-9)
    return 0;

  const auto x = p[1];
  return 0.25 * x * (1 - x) * Chebyshev<deg - 2>::eval(2 * (x - 0.5));
}

template <>
double ChebyshevCurrent<0>::value(
    const dealii::Point<2> &/*p*/,
    const unsigned int /*component*/) const
{
  return 0;
}

template <>
double ChebyshevCurrent<1>::value(
    const dealii::Point<2> &/*p*/,
    const unsigned int /*component*/) const
{
  return 0;
}

#endif // CHEBYSHEV_CURRENT_H