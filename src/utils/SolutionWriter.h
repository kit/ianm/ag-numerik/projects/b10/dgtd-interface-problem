#ifndef SOLUTION_WRITER_H_
#define SOLUTION_WRITER_H_

#include <filesystem>
#include <iostream>
#include <vector>

#include "deal.II/base/data_out_base.h"
#include "deal.II/numerics/data_out.h"
#include "deal.II/numerics/data_out_dof_data.h"
#include "deal.II/dofs/dof_handler.h"

namespace fs = std::filesystem;

template<typename VectorType>
class SolutionWriter
{
private:
    using vector_type = VectorType;
    const fs::path output_path;
    const std::string name;
    const unsigned int patch_degree;

    const fs::path dir_path;
    std::vector<std::pair<double, std::string>> times_and_names;
    
    dealii::DataOut<2> data_out;
    int saved_vectors = 0;


    // TE Mode
    const std::vector<std::string> vector_names{ "H", "E", "E" };
    const std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation>
        vector_interpretation{ dealii::DataComponentInterpretation::component_is_scalar,
        dealii::DataComponentInterpretation::component_is_part_of_vector,
        dealii::DataComponentInterpretation::component_is_part_of_vector };

public:
    SolutionWriter(
        const fs::path& out_path,
        const std::string name,
        const dealii::DoFHandler<2>& dof_handler,
        const unsigned int patch_degree);
    ~SolutionWriter();

    void write_solution(const VectorType& vector, const double time, dealii::DoFHandler<2>& dof_handler);

};

template<typename VectorType>
SolutionWriter<VectorType>::SolutionWriter(
    const fs::path& out_path,
    const std::string name,
    const dealii::DoFHandler<2>& dof_handler,
    const unsigned int patch_degree)
    : output_path{out_path},
    name{name},
    patch_degree{patch_degree},
    dir_path{output_path / name}
{
    // create path if it does not exists
    fs::create_directories(dir_path);

    // create data out object
    data_out.attach_dof_handler(dof_handler);

}

template<typename VectorType>
SolutionWriter<VectorType>::~SolutionWriter()
{
    std::stringstream filename;
    filename << name << ".pvd";
    fs::path pvd_path = dir_path / filename.str();

    std::ofstream pvd_stream(pvd_path);
    dealii::DataOutBase::write_pvd_record(pvd_stream, times_and_names); 
}

template<typename VectorType>
void SolutionWriter<VectorType>::write_solution(
    const VectorType& save_vector, 
    const double time,
    dealii::DoFHandler<2>& dof_handler) {
    data_out.add_data_vector(
        dof_handler,
        save_vector, 
        vector_names,
        vector_interpretation);
    data_out.build_patches(patch_degree);

    std::stringstream filename;
    filename << name << "_" << saved_vectors << ".vtu";

    auto file_path = dir_path / filename.str();

    std::ofstream file_stream(file_path);
    data_out.write_vtu(file_stream);

    times_and_names.emplace_back(time, filename.str());

    saved_vectors++;
}


#endif // SOLUTION_WRITER_H_