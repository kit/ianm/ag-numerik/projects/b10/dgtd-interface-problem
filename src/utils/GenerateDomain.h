#ifndef GENERATE_DOMAIN_H_
#define GENERATE_DOMAIN_H_

#include "deal.II/grid/tria.h"
#include "deal.II/grid/grid_generator.h"
#include "deal.II/grid/grid_tools.h"
#include "deal.II/base/point.h"

#include "Distort.h"

/**
 * @brief Generated domain and distort the mesh.
 *  Only the interface alignment in x direction gets preserved.
 *  The midpoint does not get preserved.
 *
 *  ---------p1-------p2
 *  |        |        |
 *  |        |        |
 *  |        |        |
 *  p0-----------------
 *
 * @param triangulation
 * @param p0 Lower left point first square
 * @param p1 Upper right point first square
 * @param p2 Upper right point second square
 * @param mesh_size
 * @param distort_factor Distortion factor should be well below 0.5
 */
void assemble_grid_xy_distortion(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double distort_factor);

/**
 * @brief Like @assemble_grid_xy_distortion but preserves the midpoint
 * 
 * @param triangulation 
 * @param p0 Lower left point first square
 * @param p1 Upper right point first square
 * @param p2 Upper right point second square
 * @param mesh_size 
 * @param distort_factor 
 */
void assemble_gird_preserve_midpoint(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double distort_factor);

/**
 * @brief Generated domain and distort the mesh.
 *  Only the interface alignment in x direction gets preserved.
 *  The midpoint does not get preserved.
 *
 *  ---------p1-------p2
 *  |     /  |  \     |
 *  |   /    |    \   |
 *  | /      |      \ |
 *  p0-----------------
 *
 * @param triangulation
 * @param p0 Lower left point first square
 * @param p1 Upper right point first square
 * @param p2 Upper right point second square
 * @param mesh_size
 * @param distort_factor Distortion factor should be well below 0.5
 */
void assemble_grid_via_triangles(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double distort_factor = 0.0);

void assemble_grid_via_gmsh(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double mesh_size_interface);

void assemble_grid_via_gmsh_via_triangles(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double mesh_size_interface);

void assemble_grid_via_8_triangles(
    dealii::Triangulation<2>& triangulation,
    const dealii::Point<2>& p0,
    const dealii::Point<2>& p1,
    const dealii::Point<2>& p2,
    const double mesh_size,
    const double distort_factor = 0.0);

/**
 * @brief Refines the triangulation at the interface.
 *  Assumes that the cells are marked
 * 
 * @param triangulation 
 * @param refinements
 * @param material_left
 * @param material_right
 */
void refine_interface(
    dealii::Triangulation<2>& triangulation,
    const unsigned int refinements,
    const dealii::types::material_id material_left = 0,
    const dealii::types::material_id material_right = 1,
    const double x_mid = 1.);

/**
 * @brief Mark the cells in the left square with material_left
 *  and in the right square with material_right.
 *
 * @param triangulation
 * @param material_left
 * @param material_right
 */
void mark_cells(
    dealii::Triangulation<2>& triangulation,
    const dealii::types::material_id material_left = 0,
    const dealii::types::material_id material_right = 1,
    const double x_mid = 1.);

void refine_down_to_mesh_size(
    dealii::Triangulation<2>& triangulation,
    const double mesh_size);

void random_refiner(
    dealii::Triangulation<2>& triangulation,
    const double probability);

#endif //GENERATE_DOMAIN_H_