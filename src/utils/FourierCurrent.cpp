#include <cmath>
#include <math.h>
#include <stdexcept>

#include "quadmath.h"

#include "FourierCurrent.h"


FourierCurrent::FourierCurrent(
  unsigned int exponent,
  double alpha,
  std::function<double(double)> time_modulation,
  FourierCurrent::Method method,
  double epsilon,
  double initial_time)
  : dealii::Function<2, double>(2, initial_time),
  exponent{ exponent },
  alpha{ alpha },
  time_modulation{ time_modulation },
  method{ method },
  epsilon{ epsilon },
  N{ static_cast<int>(std::pow(2, exponent)) },
  N_half{ static_cast<int>(std::pow(2, exponent - 1)) },
  in{ (fftwq_complex*)(fftwq_malloc(sizeof(fftwq_complex) * N)) },
  out{ (fftwq_complex*)(fftwq_malloc(sizeof(fftwq_complex) * N)) },
  plan{ fftwq_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE) },
  back_plan{  fftwq_plan_dft_1d(N, out, in, FFTW_FORWARD, FFTW_ESTIMATE) },
  rd{ "/dev/urandom" },
  //gen(rd()),
  gen(568365),
  dis(0.0, 1.0) {

  // Generate fourier coefficients according to a specific rule
  switch (method)
  {
  case FourierCurrent::Random:
    generate_random();
    fftwq_execute(plan);
    normalization = calculate_l2_norm();
    break;
  case FourierCurrent::Dirichlet:
    generate_dirichlet();
    fftwq_execute(plan);
    for(int idx = 0; idx < N; ++idx)
    {
      out[idx][0] = out[idx][1];
      out[idx][1] = 0;
    }
    fftwq_execute(back_plan);
    fftwq_execute(plan);
    normalization = calculate_l2_norm();
    break;
  case FourierCurrent::SymmetricDirichlet:
    generate_symmetric_dirichlet();
    fftwq_execute(plan);
    normalization = calculate_l2_norm();
    break;
  case FourierCurrent::SymmetricDirichletNew:
    generate_symmetric_dirichlet_new();
    fftwq_execute(plan);
    normalization = calculate_l2_norm();
    break;
  case FourierCurrent::SymmetricDirichletFixed:
    generate_symmetric_dirichlet_fixed();
    fftwq_execute(plan);
    normalization = calculate_l2_norm();
    break;
  default:
    throw std::domain_error("Used FourierCurrent::Method not implemented.");
    break;
  }
}

FourierCurrent::~FourierCurrent() {
  fftwq_free(in);
  fftwq_free(out);
  fftwq_destroy_plan(plan);
}

double FourierCurrent::value(const dealii::Point<2>& p, const unsigned int component) const {
  //std::cout << "im here\n";
  if (component == 0 || std::abs(p[0] - 1) > 1e-9) {
    return 0;
  }

  const auto y = 2 * M_PI * p[1] - M_PI;

  //Assert((0.0 <= y && y <= 1.0), "Out of range!");

  // calculate the index
  const int lower_idx = static_cast<int>(std::floor((N * y) / (2 * M_PI)));
  const int upper_idx = lower_idx + 1;

  //Assert((0 <= lower_idx && upper_idx < N), "Out of index!");

  const double theta = y - (lower_idx * 2 * M_PI) / N;

  const auto lower_value = get_space_value_real(lower_idx);
  const auto upper_value = get_space_value_real(upper_idx);

  // time modulation
  auto time = this->get_time();
  auto time_value = time_modulation(time);

  // return linear interpolation
  auto ret = (((1 - theta) * lower_value + theta * upper_value) * time_value);
  //std::cout << ret << "\n";
  return ret;
}

void FourierCurrent::output_numpy(std::ostream& out_stream, const unsigned int precision) {

  out_stream.precision(precision);

  // output x values
  for (int j = -N_half; j < N_half; j++)
    out_stream << (2 * M_PI * j) / N << ' ';
  out_stream << '\n';

  // output real values
  for (int j = -N_half; j < N_half; j++)
    out_stream << (long double) get_space_value_real(j) << ' ';
  out_stream << '\n';

  // output imag values
  for (int j = -N_half; j < N_half; j++)
    out_stream << (long double) get_space_value_imag(j) << ' ';
  out_stream << '\n';

  // output coefficients
  for (int j = 0; j < N; j++)
    out_stream << (long double) in[j][0] << ' ';
  out_stream << "\n";
  // output coefficients
  for (int j = 0; j < N; j++)
    out_stream << (long double) in[j][1] << ' ';
  out_stream << "\n";

  // output transform
  for (int j = 0; j < N; j++)
    out_stream << (long double) out[j][0] << ' ';
  out_stream << "\n";
  // output transform
  for (int j = 0; j < N; j++)
    out_stream << (long double) out[j][1] << ' ';
  out_stream << std::flush;
}

void FourierCurrent::generate_random() {

  const __float128 div_exponent = -0.5 * (0.5 + alpha + epsilon);
  //std::uniform_real_distribution<long double> full_dis{-1., 1.};

  // positive coefficients 0 -> N/2 -1
  for (int j = 0; j < N; j++) {
    auto sign = std::pow(-1, j);
    long int real_index = j - N_half;
    // real number
    in[j][0] = sign * dis(gen) * powq(static_cast<__float128>(1. + real_index * real_index), div_exponent);
    // complex number
    in[j][1] = 0;
  }
}

void FourierCurrent::generate_dirichlet() {

  generate_random();

  in[N_half][0] = 0;

  for (int j = 0; j < N; j++) {
    if (j == N_half) continue;
    in[N_half][0] -= in[j][0];
  }
}

void FourierCurrent::generate_symmetric_dirichlet() {

  const __float128 div_exponent = -0.5 * (0.5 + alpha + epsilon);
  for (int j = 0; j < N; j++) {
    if (j == 0 || j == N_half) {
      in[j][0] = 0;
      in[j][1] = 0;
    }
    else if (j < N_half - 1) {
      auto sign = std::pow(-1, j);
      long real_index = j - N_half;
      in[j][0] = 0;
      in[j][1] = sign * dis(gen) * powq(static_cast<__float128>(1. + real_index * real_index), div_exponent);
    }
    else {
      in[j][0] = 0;
      in[j][1] = -in[N - j][1];
    }
  }
}

void FourierCurrent::generate_symmetric_dirichlet_new() {

  const __float128 div_exponent = -0.5 * (0.5 + alpha + epsilon);
  std::uniform_real_distribution<long double> full_dis{-1., 1.};

  //for (int j = 0; j < N; j++) {
  //  if (j == 0 || j == N_half) {
  //    in[j][0] = 0;
  //    in[j][1] = 0;
  //  }
  //  else if (j < N_half - 1) {
  //    auto sign = std::pow(-1, j);
  //    long real_index = j - N_half;
  //    in[j][0] = 0;
  //    in[j][1] = sign * full_dis(gen) * powq(static_cast<__float128>(1. + real_index * real_index), div_exponent);
  //  }
  //  else {
  //    in[j][0] = 0;
  //    in[j][1] = -in[N - j][1];
  //  }
  //}

  for (int j = 0; j < N; j++) {
    if (j == 0 || j == N_half) {
      in[j][0] = 0;
      in[j][1] = 0;
    }
    else if (j < N_half - 1) {
      in[j][0] = 0;
      in[j][1] = full_dis(gen);
    }
    else {
      in[j][0] = 0;
      in[j][1] = -in[N-j][1];
    }
  }

  for (int j = 0; j < N; j++) {
    auto value = in[j][1];
    auto sign = std::pow(-1, j);
    long real_index = j - N_half;
    in[j][1] = sign * value * powq(static_cast<__float128>(1. + real_index * real_index), div_exponent);
  }
}

void FourierCurrent::generate_symmetric_dirichlet_fixed() {

  const __float128 div_exponent = -0.5 * (0.5 + alpha + epsilon);

  for (int j = 0; j < N; j++) {
    if (j == 0 || j == N_half) {
      in[j][0] = 0;
      in[j][1] = 0;
    }
    else if (j < N_half - 1) {
      in[j][0] = 0;
      in[j][1] = 1.0;
    }
    else {
      in[j][0] = 0;
      in[j][1] = -in[N-j][1];
    }
  }

  for (int j = 0; j < N; j++) {
    auto value = in[j][1];
    auto sign = std::pow(-1, j);
    long real_index = j - N_half;
    in[j][1] = sign * value * powq(static_cast<__float128>(1. + real_index * real_index), div_exponent);
  }
}

__float128 FourierCurrent::calculate_l2_norm() {
  __float128 norm = 0;

  for (int j = 0; j < N; j++) {
    norm += in[j][0] * in[j][0];
    norm += in[j][1] * in[j][1];
  }

  return sqrtq(2 * M_PI * norm);
}

__float128 FourierCurrent::max_abs_imag_value() {
  __float128 max_value = 0.0;

  for (int idx = 0; idx < N; ++idx) {
    __float128 value = fabsq(out[idx][1]);
    if (max_value < value) max_value = value;
  }

  std::cout << (long double) max_value << "\n";
  return max_value;
}
