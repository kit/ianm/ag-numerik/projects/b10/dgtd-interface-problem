
add_library(utils
  FourierCurrent.h
  FourierCurrent.cpp
  ReferenceSolution.h
  ReferenceSolution.hh
  DomainIndicatorFunction.h
  Distort.h
  SimplicialMesh.h
  SimplicialMesh.cpp
  LiftOperator.h
  LiftOperator.cpp
  ChebyshevCurrent.h
  SinusCurrent.h
  SinusCurrent.cpp
  ExponentialCurrent.h
  ExponentialCurrent.cpp
  GenerateDomain.h
  GenerateDomain.cpp
  PointExcitation.h
  PointExcitation.cpp
  SolutionWriter.h
  PolynomialSolutionQM.h
  PolynomialSolutionQM.cpp)

target_include_directories(utils PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(utils tools)

DEAL_II_SETUP_TARGET(utils)

