#ifndef DOMAIN_INDICATOR_FUNCTION_H
#define DOMAIN_INDICATOR_FUNCTION_H

class DomainIndicatorFunction : public dealii::Function<2, double>
{
public:
  // domain=0 => left; domain=1 => right
  DomainIndicatorFunction(int domain);

  double value(const dealii::Point<2> &point, const unsigned int component = 0) const override;

private:
  int domain;
};

DomainIndicatorFunction::DomainIndicatorFunction(int domain)
    : Function(1, 0),
      domain(domain) {}

double DomainIndicatorFunction::value(const dealii::Point<2> &p, const unsigned int /*component*/) const
{
  if (p[0] <= 1 && domain == 0)
  {
    return 1;
  }
  else if (p[0] >= 1 && domain == 1)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

//class StripIndicatorFunction : public dealii::Function<2, double>
//{
//  double value(const dealii::Point<2> &point, const unsigned int component = 0) const
//  {
//    if (std::abs(point[0] - 1) < 0.1)
//      return 1;
//    else
//      return 0;
//  }
//}

#endif //DOMAIN_INDICATOR_FUNCTION_H