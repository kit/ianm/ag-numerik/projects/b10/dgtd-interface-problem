# DGTD FOR LINEAR MAXWELL EQUATIONS WITH INHOMOGENEOUS INTERFACE

This code was used for the numerical experiments in the paper

> ERROR ANALYSIS OF DGTD FOR LINEAR MAXWELL EQUATIONS WITH INHOMOGENEOUS INTERFACE CONDITIONS
>
> By B. Dörich, J. Dörner, M. Hochbruck

This software is published in accordance with the guidelines for safeguarding good research practice and thus serves to reproduce the experiments in the above-mentioned publication. 

## Requirements

The code is written in C++17 and uses the software packages
    
*   [deal.II](https://www.dealii.org/), Version 9.5.0
*   [CMake](https://cmake.org/), Version 3.22.1
*   [TiMaxdG](https://gitlab.kit.edu/kit/ianm/ag-numerik/projects/dg-maxwell/timaxdg), Commit #2002f893
*   [fftw](https://www.fftw.org/) Version 3.x.x (with quadmath)

Note that the authors of this paper develope the software package TiMaxdG.
Therefore, the source code is provided within this repository and uses of the main branch with the hash #2002f893.

For plot generation, Python 3 is used with the usual scientific software stack (numpy, scipy, matplotlib ...).
Any reasonable new distribution works. 

For convenience, we provide a reproduction environment via [docker](https://www.docker.com/) and [enroot](https://github.com/NVIDIA/enroot).
For that, please consider the installation instructions for the tools and the files:

*   .devcontainer/Dockerfile
*   .devcontainer/README.md
*   .devcontainer/ENROOT.md

## Reproduction

### Figure 2:

The experiment is build with the commands
```bash

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    cd src
    make CavitySolution_Prototype
```

The experiment is executed with the commands
```bash
    ./CavitySolution_Prototype
```
The experiment produces a tabular file `error.txt` with the results and a folder structure with additional run artifacts.

The plot is generated with
```bash
    python3 ../../python/CavityPlots.py error.txt
```
The output file `cavity_error_plot.pdf` is produced.

### Figure 4:

The experiment is build with the commands
```bash

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    cd src
    make ReferenceSolution_Prototype 
```

The experiment is executed with the commands
```
    mkdir artifacts
    cd artifacts
    ../ReferenceSolution_Prototype ../../../src/config.json
    cd ..
    ../../python/postprocess.py artifacts/
```
The experiment produces a tabular file `error.txt` with the results.


The plots are generated with
```bash
    python3 ../../python/ReferenceErrorPlots.py error.txt
```
The output files `full_error_deg_1pdf` and `full_error_deg_2.pdf` are produced.

**Note that this experiment is computationally very costly and the calculations should be distributed on several high performance computers!**

### Figure 5

The experiment is build with the commands
```bash

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    cd src
    make PolynomialSolution_Prototype
```

The experiment is executed with the commands
```bash
    ./PolynomialSolution_Prototype
```
The experiment produces a tabular file `output/error.txt` with the results and a folder structure `output/` with additional run artifacts.

The plot is generated with
```bash
    python3 ../../python/PolynomialErrorPlot.py output/error.txt
```
The output file `polynomial_error_plot.pdf` is produced.