#!/usr/bin/env python
# coding: utf-8

import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#import tikzplotlib



path = sys.argv[1]
data = pd.read_csv(path, delimiter='\t')



def gen_order_line(steps, errors, deg, offset=1.0):
    return (steps, (steps**deg) / (steps[-1] ** deg) * offset * min(errors))
def order_estimate(seq, step):
    upper = np.log(seq[1:]/seq[:-1])
    lower = np.log(step[1:]/step[:-1])
    return upper/lower


mesh_sizes = data['max_mesh_size'].unique()
order_line_gererated = False
for mesh_size in mesh_sizes:
    mesh_size_data = data.where(data['max_mesh_size'] == mesh_size).dropna()
    taus = mesh_size_data['time_step_width'].to_numpy(np.float64)
    errors = mesh_size_data['max_l2_error_nodal'].to_numpy(np.float64)
    errors[errors < 6e-16] = 1000
    errors[errors > 1] = 1000
    plt.loglog(taus,errors, marker='*',label=f"$h={mesh_size:.5f}$")
    plt.ylim(1e-8,1)

    if not order_line_gererated:
        line = gen_order_line(taus, errors, 2, offset=0.5)
        plt.loglog(*line, color='grey', linestyle='dashed')
        order_line_gererated = False
plt.xlabel(r"$\tau$")
plt.ylabel(r"$\max_n \|\mathbf{u}^n_h - \mathbf{u}(t_n)\|_{L^2(Q)^6}$")
plt.legend(loc='center left', bbox_to_anchor=(1.09, 0.5))
plt.grid(True)

plt.tight_layout()
plt.savefig("polynomial_error_plot.pdf", bbox_inches='tight')
#tikzplotlib.save("polynomial_error_plot.tex")
