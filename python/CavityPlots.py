#!/usr/bin/env python
# coding: utf-8

# In[1]:

import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#import tikzplotlib
import pandas as pd

matplotlib.rc('text', usetex=False)



path = sys.argv[1]
data = pd.read_csv(path, delimiter='\t')


degrees = data['degree'].unique()



def gen_order_line(steps, errors, deg, offset=1, idx = -1):
    return (steps, (steps**deg)/(steps[idx]**deg)*offset*errors[idx])

def order_estimate(seq, step):
    upper = np.log(seq[1:]/seq[:-1])
    lower = np.log(step[1:]/step[:-1])
    return upper/lower



figure, axes = plt.subplots()
for deg in degrees:
    #print(f"degree = {deg}")
    degree_data = data.where(data['degree'] == deg).dropna()
    #print(deg_table)
    h_min = degree_data['min_mesh_size'].to_numpy(np.float64)
    #print(h_min)
    h_max = degree_data['max_mesh_size'].to_numpy(np.float64)
    #print(h_max)
    # Use h_max!
    h = h_max
    
    tau = degree_data['time_step_width'].to_numpy(np.float64)
    err_nodal = degree_data['max_l2_error_nodal'].to_numpy(np.float64)
    err_modal = degree_data['max_l2_error_modal'].to_numpy(np.float64)
    err_diff = degree_data['max_l2_error_diff'].to_numpy(np.float64)
    
    order_line = gen_order_line(h, err_nodal, deg, 1.8, 0)
    diff_order_line = gen_order_line(h, err_diff, deg+0.5, 1, 0)
    #estimated_order = np.mean(order_estimate(err_nodal, h))
    estimate_function = lambda x: np.mean(x)
    estimated_order_nodal = estimate_function(order_estimate(err_nodal, h))
    estimated_order_modal = estimate_function(order_estimate(err_modal, h))
    estimated_order_diff = estimate_function(order_estimate(err_diff, h))
    
    axes.loglog(h, err_nodal, marker='o', markersize=6, 
                label=f"nodal, $k = {int(deg)}$, O({estimated_order_nodal:.2f})")
    axes.loglog(h, err_modal, marker='*', markersize=3, 
                label=f"modal, $k = {int(deg)}$, O({estimated_order_modal:.2f})")
    #axes.loglog(h, err_diff, marker='+', markersize=3, 
    #            label=f"diff, $k = {int(deg)}$, O({estimated_order_diff:.2f})")

    plt.loglog(order_line[0],order_line[1], '--', color="gray", label=f"O({deg})")
    #plt.loglog(diff_order_line[0], diff_order_line[1], '--', color="blue", alpha=0.2, label=f"O({deg+0.5})")

    #estimated_order_line = gen_order_line(h, err_nodal, estimated_order)
    #plt.loglog(estimated_order_line[0], 
    #           estimated_order_line[1]*0.6, 
    #          '--', color="gray", 
    #          label=f"{estimated_order}")


axes.grid(True)
axes.set_xlabel(r"$h$")
axes.set_ylabel(r"$\max_n \|\mathbf{u}^n_h - \mathbf{u}(t_n)\|_{L^2(Q)}$")
axes.legend(loc='center left', bbox_to_anchor=(1.09, 0.5))
axes.set_title(f"Cavity solution, $\\tau = {tau[0]}$")

plt.tight_layout()
plt.show()

figure.savefig("cavity_error_plot.pdf", bbox_inches='tight')
#tikzplotlib.save("cavity_error_plot.tex")

