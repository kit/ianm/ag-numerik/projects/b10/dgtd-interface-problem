#!/bin/python3

import os
import re
import sys
import struct



pat = re.compile('alpha_(\d+(.\d+)?)')
degree_pat = re.compile('degree_(\d)')

def do(path, output_file):


    all_files = os.listdir(path)


    for dir in sorted(filter(pat.match, all_files)):
        alpha = pat.findall(dir)[0][0]
        print("Found alpha: " + alpha)
    
        alpha_files = os.listdir(path + '/' + dir)
        for deg_dir in filter(degree_pat.match, alpha_files):
            deg = degree_pat.findall(deg_dir)
            
            local_path = path + '/' + dir + '/' + deg_dir + '/error.txt'
            #print(path)
            with open(local_path) as open_file:
                for line in open_file:
                    #print(alpha)
                    new_line = alpha + '\t' + line
                    output_file.write(new_line)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Nothing to do!")
    else:
        with open('error.txt', 'w') as file:
            for i in range(1,len(sys.argv)):
                do(sys.argv[i], file)
