#!/usr/bin/env python
# coding: utf-8

import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.fft as fft
#import tikzplotlib
get_ipython().run_line_magic('matplotlib', 'inline')


table = np.loadtxt(sys.argv[1])


keys = np.sort(np.unique(table[:,0]))

print(keys)


key_table = {}
for key in keys:
    key_table[key] = table[table[:,0] == key]


def order_estimate(seq, step):
    upper = np.log(seq[1:]/seq[:-1])
    lower = np.log(step[1:]/step[:-1])
    return upper/lower


def gen_order_line(steps, errors, deg, offset=1):
    return (steps, (steps**deg)/(steps[0]**deg)*offset*max(errors))


def plot_deg_data(deg_data, alpha):
    h_range = deg_data[:,2][:]
    interface_errors = deg_data[:,4][:]
    plt.loglog(h_range, interface_errors, label = f"$a={alpha}$", linestyle="--", marker="*")
    #order_seq = order_estimate(interface_errors, h_range)
    #order = np.median(order_seq)
    order = np.polyfit(np.log(h_range), np.log(interface_errors), 1)[0]
    plt.loglog(*gen_order_line(h_range, interface_errors, order), alpha=0.3, label = f"$O({order:.3})$")
    #plt.loglog(*gen_order_line(h_range, interface_errors, 1))#, label = f"$O({alpha})$")


def print_deg(deg):

    for key in keys:
        if key <= 1.5:
            print(key)
            continue
        data = key_table[key][:,1:]
        data = data[data[:,0] == deg]
        plot_deg_data(data, key)
    plt.legend()
    plt.legend(loc='upper left', bbox_to_anchor=(1., 1))
    plt.title(f"Degree = {deg}")
    plt.xlabel("mesh size")
    plt.ylabel("error")
    plt.show()



def plot_alpha_order(deg):
    orders = []

    for key in keys:
        data = key_table[key][:,1:]
        data = data[data[:,0] == deg]
        h_range = data[:,2][:]
        errors = data[:,4][:]
        order_seq = order_estimate(errors, h_range)
        order = np.mean(order_seq)
        #order = np.min(order_seq)
        #order = np.median(order_seq)
        orders.append(order)

    
    # ref lines
    xs = np.linspace(np.min(keys), np.max(keys), 1000)
    xs = xs[xs >= 0.5]
    plt.plot(xs,np.clip(xs-0.5, 0, None),color="orange", label="O(1)")
    plt.title(f"$k = {deg}$")
    plt.xlabel(r"$\alpha$")
    plt.ylabel("Convergence order")
    plt.ylim(0.0,2.0)
    #plt.grid()
    if deg == 1:
        plt.plot(keys, 0.88*np.ones_like(keys), linestyle = "-.", color="red", label="y=0.88")
    elif deg == 2:
        plt.plot(keys, 1.1*np.ones_like(keys), linestyle = "-.", color="red", label="y=1.1")
    plt.plot(keys,np.ones_like(keys)*(1), color="red", alpha=0.5, linestyle="--", label=r"$y=1$")
    plt.plot(np.ones_like(keys)*(0.5), keys, color="red", alpha=0.5, linestyle="dotted", label=r"$x=0.5$")
    plt.plot(keys, orders, marker="o", markersize=3)
    plt.legend(loc='center left', bbox_to_anchor=(1.09, 0.5))


plot_alpha_order(1)
plt.tight_layout()
plt.savefig("full_error_deg_1.pdf")
#tikzplotlib.clean_figure()
#tikzplotlib.save("full_error_deg_1.tex")


plot_alpha_order(2)
plt.tight_layout()
plt.savefig("full_error_deg_2.pdf")
#tikzplotlib.clean_figure()
#tikzplotlib.save("full_error_deg_2.tex")

