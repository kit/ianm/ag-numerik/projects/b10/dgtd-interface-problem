TiMaxdG is being developed at Karlsruhe Institute of Technology, Germany. 
The code is currently maintained and developed by

* Constantin Carle
* Julian Dörner

The following people provided substantial contributions in the past

* Jonas Köhler
* Jan Leibold
* Bernhard Maier
